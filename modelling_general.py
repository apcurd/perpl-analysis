# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 15:06:11 2018

@author: fbsacu
"""

import numpy as np
from scipy.special import i0
from scipy import integrate

def paircorr3d(r, rmean, sigma):
    """
    Apparent separations(r) for two repeatedly localised fluorophores in 2D
    with true separation rmean.
    sigma = sum in quadrature of sigma for each fluorophore,
    so sigma ** 2 = 2 * loc.prec ** 2. for repeated locs of the same molecule.
    From Churchman, Biophys J 90, 668-671 (2006).
    Need Bessel function, imported as i0(x)
    """
    if rmean == 0:
        """Using approximation of sinh from
        http://mathworld.wolfram.com/SeriesExpansion.html"""
        p = np.sqrt(2 / np.pi) * (r ** 2 / sigma ** 3) * (
                np.exp(-(rmean ** 2 + r ** 2) / (2 * sigma ** 2))
                )
    #if rmean < (sigma * 5.):
    elif (np.max(r) * rmean / sigma ** 2) < 700.:
        p = np.sqrt(2 / np.pi) * (r / (sigma * rmean)) * (
             np.exp(-(rmean ** 2 + r ** 2) / (2 * sigma ** 2)) * 
             np.sinh(r * rmean / sigma ** 2))
    else: # Approximate overly large sinh()
        p =  1. / 2. * np.sqrt(2 / np.pi) * (r / (sigma * rmean)) * np.exp(
                    -((r - rmean) ** 2) / (2 * sigma ** 2))
    return p

def paircorr2d(r, rmean, sigma):
    """
    Apparent separations(r) for two repeatedly localised fluorophores in 2D
    with true separation rmean.
    sigma = sum in quadrature of sigma for each fluorophore,
    so sigma ** 2 = 2 * loc.prec ** 2. for repeated locs of the same molecule.
    From Churchman, Biophys J 90, 668-671 (2006).
    Need Bessel function, imported as i0(x)
    """
    #if rmean < (sigma * 5.):
    if (np.max(r) * rmean / sigma ** 2) < 700.:
        p = (r / sigma ** 2) * (
             np.exp(-(rmean ** 2 + r ** 2) / (2 * sigma ** 2)) * 
             i0(r * rmean / sigma ** 2))
    else: # Approximate overly large i0()
        p = 1 / (np.sqrt(2 * np.pi) * sigma) * np.sqrt(r / rmean) * np.exp(
                    -((r - rmean) ** 2) / (2 * sigma ** 2))
    return p

def paircorr1d(z, zmean, sigma):
    """
    Apparent separations(z) for two repeatedly localised fluorophores
    with true separation rmean.
    Sigma = sum in quadrature of sigma for each fluorophore,
    so sigma ** 2 = 2 * loc.prec ** 2. for repeated locs of the same molecule.
    From Churchman, Biophys J 90, 668-671 (2006).
    Need Bessel function, imported as j0(x)
    """

    if zmean < (sigma * 10.):    
        p = np.sqrt(2 / np.pi) * 1 / sigma * (
                np.exp(-(zmean ** 2 + z ** 2) / (2 * sigma ** 2))
                * np.cosh(zmean * z / sigma ** 2)
                )
    else: # Approximate overly large np.cosh()
        """ Old version - mistakenly kept z/zmean factor from 2D case
        in multiplier. Obviously, this is 1 at z=zmean. """
        # p = 1. / 2. * np.sqrt(2 / np.pi) * 1 / sigma * np.sqrt(z / zmean) * (
        #        np.exp(-((z - zmean) ** 2) / (2 * sigma ** 2))
        #        )
        """ New version - removed z/zmean in multiplier. """
        p = 1. / 2. * np.sqrt(2 / np.pi) * 1 / sigma * (
                np.exp(-((z - zmean) ** 2) / (2 * sigma ** 2))
                )
    return p

def gauss1d(x, xmean, sigma):
    """Apparent separations(z) for two repeatedly localised fluorophores
    with true separation rmean, when zmean >> sigma.
    Sigma = sum in quadrature of sigma for each fluorophore.
    """
    p = np.sqrt(2 / np.pi) * 1 / sigma * (
            np.exp(-((x - xmean) ** 2) / (2 * sigma ** 2))
            )
    return p

def bootstrap(data, repeats):
    """Generate bootstrap resamples of data.
    
    Example of use including confidence intervals:
    
        axsmoothb = np.zeros((reps, axlength))
        
        axb = np.zeros((reps, axlength))
        
        for i in range(reps):
        
            hist, edges = np.histogram(boots[i], bins=(np.arange(0, axlength + 1, 1)))
            
            axb[i] = hist / 2. # Remove duplicates
            
            axsmoothb[i] = gaussian_filter1d(axb[i], 4.8)

        
        axsmsort = np.sort(axsmoothb, axis=0)
        
        plt.plot(axsmooth, '-', axsmsort[reps/20], '-', axsmsort[reps * 19/20], '-')
    """
    boots = np.random.choice(data, (repeats, len(data)))
    return boots

def linfit(x, slope, offset):
    rpd = offset + slope * x
    return rpd

def generatecdf(sorteddata):
    """This is how you can get a cdf from the exptl data.
    It does not include a (0, 0) point."""
    cdf = np.zeros(len(sorteddata))
    for i in np.arange(len(cdf)):
        cdf[i] = (i + 1) * 1. / len(sorteddata)
    return cdf

def lincdf(x, xmin, xmax, slope, offset):
    """All of x must be between xmin and xmax for cdf to be from 0 to 1."""
    # pdf = offset + slope * x
    cdf = offset * (x - xmin) + slope * (x ** 2 - xmin ** 2) / 2
    cdf = cdf / (offset * (xmax - xmin) + slope * (xmax ** 2 - xmin ** 2) / 2)
    return cdf

def rejectionsample(pdf, params, pdfint, xmin, xmax, N):
    """Generate random sample from pdf using von Neumann's rejection
    sampling. See Bayesian Data Analysis (Gelman, Carlin, Stern, Rubin),
    2nd Ed., p284-5.
    Will normalise pdf and use uniform function, g(f) = 1, as approximation
    function, and constant M = 10 (for instance),
    so pdf(x)/g
    Args:
        pdf: The probability density function (this does not need to be
            normalised)
        args: Arguments to the probability density function
        xmin: Minimum bound for the simulated data.
        xmax: Maximum bound for the simulated data.
        N: Number of data points in the final sample.
    Returns:
        xsim: Simulated random data from pdf
        totattempts: How many random samples were taken in total to get to
            xsim.
    """
    #M = 1. # Increases how many samples will be needed
           # and how good the sampling will be?
    #pdfint = integrate.quad(pdf, xmin, xmax, args=tuple(params))
    xsim = np.array([])
    tries = 0
    while len(xsim) < N:
        tries = tries + 1
        u = np.random.uniform()
        xtry = np.random.uniform(xmin, xmax)
        px = pdf(xtry, *params) / pdfint
        # if u < (px / M):
        if u < px: # For M = 1.
            xsim = np.append(xsim, xtry)
    return xsim, tries

        
        
        
