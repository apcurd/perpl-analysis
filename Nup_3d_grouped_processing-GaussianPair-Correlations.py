# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 16:05:48 2018

@author: fbsacu
"""

# column [4] is locprecnm, [7] is locprecznm
# 118388 locs
# 55925 with xyprec < 5.
# 36297 with zprec < 10.

import numpy as np
from scipy.special import i0
from scipy.optimize import curve_fit
from scipy.stats import ks_2samp
from scipy.ndimage.filters import gaussian_filter1d
from scipy.ndimage.filters import gaussian_filter
import modelling_general as model
import time
from scipy import integrate
import matplotlib.pyplot as plt
from plotting import plotestci


xyz = np.loadtxt('S:/Peckham/Bioimaging2/Alistair/nupdata-JonasRies/Nup_SNAP_3D/Nup107_SNAP_3D_GRROUPED.txt', delimiter=',', skiprows=1)
x = xyz[:, 0]
y = xyz[:, 1]
z = xyz[:, 2]

xyzcentre = xyz[(x > x.mean() - 1000) & (x < x.mean() + 1000) &
                (y > y.mean() - 1000) & (y < y.mean() + 1000) &
                (z > z.mean() - 1000) & (z < z.mean() + 1000)]
# Filter for loc prec in Z
xyzcentre = xyzcentre[xyzcentre[:, 7] < 10.]

xcentre = xyzcentre[:, 0]
ycentre = xyzcentre[:, 1]
zcentre = xyzcentre[:, 2]

hxy = np.histogram2d(xcentre, ycentre,
            bins=(np.arange(xcentre.min(), xcentre.max(), 10),
                np.arange(ycentre.min(), ycentre.max(), 10)))[0].astype(float)
#hxy = gaussian_filter1d(hxy, 1)
axxy = plt.matshow(np.transpose(hxy), cmap='gray', aspect=1)

hxz = np.histogram2d(xcentre, zcentre,
            bins=(np.arange(xcentre.min(), xcentre.max(), 10),
                np.arange(zcentre.min(), zcentre.max(), 10)))[0].astype(float)
#hxz = gaussian_filter1d(hxz, 1)
axxz = plt.matshow(np.transpose(hxz), cmap='gray', aspect=1)



plt.hist2d(x, y, bins=(np.arange(x.mean() - 2000, x.mean() + 2000, 10), np.arange(y.mean() - 1000, y.mean() + 1000, 10)), cmap='inferno')


filt = xyz[xyz[:, 7] < 10.]

histrawz = np.histogram(filt[:, 2], bins=np.arange(201.))[0].astype(float)

def linear(z, m, c):
    return m * z + c

popt, pcov = curve_fit(linear, z, histrawz)
zdetrend = histrawz - linear(z, *popt)


"""How to fit curves.
This is a Gaussian example:
    
def func(x, a, b, c):
    return a * np.exp(-(x - b) ** 2 / 2 * c)

y = func(x, 2., 3., 2.)
ynoisy = y + .2 * np.random.normal(size=x.size)

popt, pcov = curve_fit(func, x, ynoisy)

plt.plot(x, func(x, popt*), '--')  

popt
Out[20]: array([1.99894135, 2.96838371, 1.75323205])

pcov
Out[21]: 
array([[4.88750682e-03, 7.42498836e-11, 5.71543997e-03],
       [7.42498836e-11, 9.30290745e-04, 1.35501175e-10],
       [5.71543997e-03, 1.35501175e-10, 2.00508694e-02]])
"""

nns = np.loadtxt('S:/Peckham/Bioimaging2/Alistair/nupdata-JonasRies/Nup_SNAP_3D/Nup107_SNAP_3D_GRROUPED_10nmZprec_PERPL-relposns_200.0filter.csv',
                 delimiter=',')

def twogauss(x, a, c, d, e, f):
    return (
        a * np.exp(-(x ** 2) / (2 * c ** 2)) +
        d * np.exp(-(x - e) ** 2 / (2 * f ** 2))
        )

def twopaircor(z, peak1sd, A, peak2mean, peak2sigma, B):
    rpd = A * paircorr1d(z, 0, peak1sd)
    rpd = rpd + B * paircorr1d(z, peak2mean, peak2sigma)
    return rpd
    
depth = 150.
z = np.arange(depth)

histz = np.histogram(np.abs(nns[:, 2]), bins=np.arange(depth + 1))[0].astype(float)

popt, pcov = curve_fit(twogauss, z, histz)

#SD of parameter estimates:
perr = np.sqrt(np.diag(pcov))

#plt.plot(z, twogauss(z, *popt))
 
"""
For Zprec < 10 nm
popt 
Out[50]: 
array([ 1.30457537e+04,  5.39564038e+01, -2.54953115e+01,  3.09953590e+04,
       -2.54115235e+00, -2.10846820e+01])

pcov
Out[47]: 
array([[ 1.65104229e+04, -6.89620248e+01, -3.29218137e+01,
        -2.82481274e+04,  2.87824198e+01,  5.23230672e+01],
       [-6.89620248e+01,  3.42618445e-01,  1.78261418e-01,
         1.28919382e+02, -1.03262806e-01, -2.29239297e-01],
       [-3.29218137e+01,  1.78261418e-01,  1.04603069e-01,
         6.59947064e+01, -4.24286392e-02, -1.12562800e-01],
       [-2.82481274e+04,  1.28919382e+02,  6.59947064e+01,
         6.87678241e+04, -7.51955979e+01, -1.05937839e+02],
       [ 2.87824198e+01, -1.03262806e-01, -4.24286392e-02,
        -7.51955979e+01,  1.17748199e-01,  1.20739236e-01],
       [ 5.23230672e+01, -2.29239297e-01, -1.12562800e-01,
        -1.05937839e+02,  1.20739236e-01,  1.87477451e-01]])
    
perr
Out[49]: 
array([128.49289059,   0.58533618,   0.32342398, 262.23619911,
         0.34314457,   0.43298666])
"""

"""
For XYprec < 5 nm
popt
Out[56]: 
array([ 3.17930852e+04,  5.28690966e+01, -2.65790678e+01,  7.12567394e+04,
       -2.36756170e+00, -2.11472771e+01])
    
pcov
Out[58]: 
array([[ 7.09454589e+04, -1.24319683e+02, -5.81731497e+01,
        -1.23674864e+05,  3.93311142e+01,  8.50250064e+01],
       [-1.24319683e+02,  2.49377658e-01,  1.25527317e-01,
         2.33625266e+02, -5.57742833e-02, -1.51875603e-01],
       [-5.81731497e+01,  1.25527317e-01,  7.00940999e-02,
         1.16495736e+02, -2.11247408e-02, -7.18376571e-02],
       [-1.23674864e+05,  2.33625266e+02,  1.16495736e+02,
         2.79941696e+05, -9.99916247e+01, -1.68503575e+02],
       [ 3.93311142e+01, -5.57742833e-02, -2.11247408e-02,
        -9.99916247e+01,  6.34729712e-02,  6.51613589e-02],
       [ 8.50250064e+01, -1.51875603e-01, -7.18376571e-02,
        -1.68503575e+02,  6.51613589e-02,  1.13660937e-01]])

perr
Out[57]: 
array([2.66355888e+02, 4.99377270e-01, 2.64752903e-01, 5.29095167e+02,
       2.51938427e-01, 3.37136377e-01])
"""

"""
correlation_matrix:
invdiag = np.diag(1/perr)
step1 = np.matmul(pcov, invdiag)
step2 = np.matmul(invdiag, step1)    
    
step2
Out[89]: 
array([[ 1.        , -0.9346496 , -0.82493465, -0.87757723,  0.58611054,
         0.94684471],
       [-0.9346496 ,  1.        ,  0.94944266,  0.88421371, -0.44331335,
        -0.90209781],
       [-0.82493465,  0.94944266,  1.        ,  0.83164026, -0.31670596,
        -0.80483307],
       [-0.87757723,  0.88421371,  0.83164026,  1.        , -0.75012807,
        -0.94464735],
       [ 0.58611054, -0.44331335, -0.31670596, -0.75012807,  1.        ,
         0.76716734],
       [ 0.94684471, -0.90209781, -0.80483307, -0.94464735,  0.76716734,
         1.        ]])
"""

extent = 200.
histxy = np.histogram(
            np.sqrt(nns[:, 0] ** 2 + nns[:, 1] ** 2),
            bins=np.arange(extent + 1))[0].astype(float)
histxyscale = histxy / (np.max(histxy) / 2) # Overly large values can
                                            # cause problems...
                                            
# BETTER:
fitlength = 200
xypoints = np.sqrt(nns[:, 0] ** 2 + nns[:, 1] ** 2)
xypoints = xypoints[(xypoints < fitlength)] 
xypoints = np.sort(xypoints)
xypoints = xypoints[::2] # Remove duplicates

# Limit XY distances to when dz < 20 nm (one layer, as indicated by z-profile)
fitlength = 200
xypoints = np.sqrt(nns[:, 0] ** 2 + nns[:, 1] ** 2)
xypoints = xypoints[(xypoints < fitlength) & (nns[:, 2] < 20)]
xypoints = np.sort(xypoints)
xypoints = xypoints[::2] # Remove duplicates

xy = plt.hist(xypoints,
            weights=np.repeat(float(fitlength) / len(xypoints), len(xypoints)),
            bins=np.arange(float(fitlength) + 1), color='lightblue')[0]  

# z-data
fitlength = 150
zpoints = nns[:, 2]
zpoints = np.sort(zpoints)
zpoints = zpoints[::2] # Remove duplicates

zhist = plt.hist(zpoints,
            weights=np.repeat(float(fitlength) / len(zpoints), len(zpoints)),
            bins=np.arange(float(fitlength) + 1), color='xkcd:lightblue', alpha=0.5)[0]

mod = twolayer
p0 = [60., 10., 10., 20., 0.01]
bounds = (0., [120., 100., 100., 100., 0.1])
popt, pcov = curve_fit(mod, z, zhist, p0=p0, bounds=bounds)
perr = np.sqrt(np.diag(pcov))
plt.plot(z, mod(z, *popt))

plt.figure()
axz = plt.subplot(111)
zhist = axz.hist(zpoints,
            weights=np.repeat(float(fitlength) / len(zpoints), len(zpoints)),
            bins=np.arange(float(fitlength) + 1), color='xkcd:lightblue', alpha=0.5)[0]
plotestci(axz, mod, popt, perr, reps=10000, fitlength=fitlength, ci=95)



# z-model
def twolayer(z, separation, A, B, sigma, offset):
    withinlayer = A * model.paircorr1d(z, 0, sigma)
    betweenlayer = B * model.paircorr1d(z, separation, sigma)
    rpd = withinlayer + betweenlayer + offset
    return rpd   
                                          

# What about xy, if dz keep within the first peak?
samez = nns[nns[:, 2] < 20]
extent = 200.
histxy = np.histogram(
            np.sqrt(samez[:, 0] ** 2 + samez[:, 1] ** 2),
            bins=np.arange(extent + 1))[0].astype(float)
histxyscale = histxy / 6000. # Overly large values can cause problems...

def generatepolypoints(n, d):
    """Generate the coordinates of points on an polygon.
    
    Args:
        n: Number of vertices.
        d: Diameter of circle on which the vertices are found.
        
    Returns:
        v: Numpy [x, y] coordinates of vertices.
    """
    v = np.zeros((n, 2))
    for a in range(n):
        v[a, 0] = d / 2 * np.cos(a * 2 * np.pi / n)
        v[a, 1] = d / 2 * np.sin(a * 2 * np.pi / n)
    
    return (v)

def get2drelposnofilt(xy):
    """Store all relative positions in a numpy array

    Args:
        xy: numpy array of localisations with shape (N, 2),
        where N is the number of localisations.
    
    Returns:
        Numpy (N, 2) array of 2D relative positions.  
    """
    relpos = xy - xy[0]  
    for i, loc in enumerate(xy[1:len(xy)]):
        relpos = np.append(relpos, xy - loc, axis=0)
    
    # Remove [0., 0.] relative positions (self-referencing)    
    relpos = relpos[np.any(relpos != 0., axis=1)]
    
    return(relpos)
    
def paircol(r, rmean, sigma):
    """
    Apparent separations(r) for two repeatedly localised fluorophores in 2D
    with true separation rmean.
    Sigma = sum in quadrature of sigma for each fluorophore.
    From Churchman, Biophys J 90, 668-671 (2006).
    Need Bessel function, imported as j0(x)
    """
    p = (r / sigma ** 2) * (
         np.exp(-(rmean ** 2 + r ** 2) / (2 * sigma ** 2)) * 
         i0(r * rmean / sigma ** 2))
    return p

def paircorr2d(r, rmean, sigma):
    """
    Apparent separations(r) for two repeatedly localised fluorophores in 2D
    with true separation rmean.
    Sigma = sum in quadrature of sigma for each fluorophore.
    From Churchman, Biophys J 90, 668-671 (2006).
    Need Bessel function, imported as i0(x)
    """
    p = (r / sigma ** 2) * (
         np.exp(-(rmean ** 2 + r ** 2) / (2 * sigma ** 2)) * 
         i0(r * rmean / sigma ** 2))
    return p

def paircorr1d(z, zmean, sigma):
    """
    Apparent separations(z) for two repeatedly localised fluorophores
    with true separation rmean.
    Sigma = sum in quadrature of sigma for each fluorophore.
    From Churchman, Biophys J 90, 668-671 (2006).
    Need Bessel function, imported as j0(x)
    """
    p = np.sqrt(2 / np.pi) * 1 / sigma * (
            np.exp(-(zmean ** 2 + z ** 2) / (2 * sigma ** 2))
            * np.cosh(zmean * z / sigma ** 2)
            )
    return p

def rpdfunc8b(r, dia, sigma, A, bg, r0):
    """
    """
    verts = generatepolypoints(8, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:7]
    rpd = np.zeros(len(r))
    for d in dists:
        rpd = rpd + A * np.exp(-(r - d) ** 2 / (2 * sigma ** 2))
    background = r * bg - bg * r0
    background[background < 0] = 0
    rpd = rpd + background
    return rpd

def rpdfuncN(r, dia, sigma, A, bg, r0):
    """
    """
    N = 6
    verts = generatepolypoints(N, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(N - 1)]
    rpd = np.zeros(len(r))
    for d in dists:
        rpd = rpd + A * np.exp(-(r - d) ** 2 / (2 * sigma ** 2))
    background = r * bg - bg * r0
    background[background < 0] = 0
    rpd = rpd + background
    return rpd

popt, pcov = curve_fit(
    rpdfuncN, r[38:], histxyscale[38:], p0=(120., 25., 1., 0.01, 50.),
    bounds=(0., [200., 50., 5., 0.1, 140.]))
ssr = np.sum((rpdfuncN(r, *popt) - histxyscale) ** 2)

"""
This is using dz < 20 nm

histxyscale = histxy / 6000. # Overly large values can cause problems...



# Reminder
perr = np.sqrt(np.diag(pcov))    
"""
      
def repeatlocsrpdtheory(r, sd, A):
    """
    From Endesfelder, Histochemistry and Cell Biology 141, 629-638 (2014).
    ********** THIS IS 2D !!!!!!!!!!!!! **************
    """
    rpd = A * r / (2 * sd ** 2) * np.exp(-(r ** 2) / (4 * sd ** 2))
    return rpd
"""
popt, pcov = curve_fit(
    repeatlocsrpdtheory, r[0:11], histxyscale[0:11])

popt = array([ 4.03994812, 18.8110679 ])
"""

def eightfoldplusreps(r, dia, sigma, A, bg, r0, sdreps, B):
    n = 8
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:4]
    contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = np.zeros(len(r))
    for i, d in enumerate(dists):
        rpd = rpd + contribs[i] * A * paircol(r, d, sigma)
    background = r * bg - bg * r0
    background[background < 0] = 0
    rpd = rpd + background
    reps = B * r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps
    return rpd

def eightfoldplusextrapeak(r, dia, sigma, A, bg, r0, sdreps, B, subr, subsd, C):
    n = 8
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:4]
    contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = np.zeros(len(r))
    for i, d in enumerate(dists):
        rpd = rpd + A * paircol(r, d, sigma) * contribs[i]
    background = r * bg - bg * r0
    background[background < 0] = 0
    rpd = rpd + background
    reps = B * r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps
    # Add possible missing peak
    rpd = rpd + C * paircol(r, subr, subsd)
    return rpd

def nfoldplusreps(r, dia, sigma, A, bg, r0, sdreps, B):
    n = 6
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n - 1)]
    rpd = np.zeros(len(r))
    for d in dists:
        rpd = rpd + A * paircorr2d(r, d, sigma)
    background = r * bg - bg * r0
    background[background < 0] = 0
    rpd = rpd + background
    reps = B * r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps
    return rpd

def nfoldplusfromdiffzplusfalsereps(r, dia, sigma, A, bg, r0, sdreps, C):
    n = 8
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n - 1)]
    rpd = np.zeros(len(r))
    for d in dists:
        rpd = rpd + A * paircorr2d(r, d, sigma)
    background = r * bg - bg * r0
    background[background < 0] = 0
    rpd = rpd + background
    samexy = A * paircorr2d(r, 0, sigma)
    rpd = rpd + samexy
    # Add 2nd set of reps
    reps2 = C * r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps2
    return rpd

# VERSION FOR FITTING WITH POINT BY POINT BACKGROUND
def nfoldplusrepsx2(r, dia, sigma, A, bg, r0, sdreps, B, subsd, C):
    n = 8
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n-1)]
    #contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = r * 0.
    for i, d in enumerate(dists):
        rpd = rpd + A * model.paircorr2d(r, d, sigma) #* contribs[i]
    background = r * bg - bg * r0
    background[background < 0] = 0
    rpd = rpd + background
    rpd = rpd + B * model.paircorr2d(r, 0., np.sqrt(2) * sdreps)

    # Add 2nd set of reps
    rpd = rpd + C * model.paircorr2d(r, 0., np.sqrt(2) * subsd)

    return rpd

# VERSION FOR INTEGRATING WITH SINGLE VALUE
def nfoldplusrepsx2int(r, dia, sigma, A, bg, r0, sdreps, B, subsd, C):
    n = 8
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n-1)]
    #contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = r * 0.
    for i, d in enumerate(dists): # Add intra-polygon distances with 
        rpd = rpd + A * model.paircorr2d(r, d, sigma) # broadening
   
    if  r > r0:
        rpd= rpd + (r - r0) * bg
                    
    # Add rep locs
    rpd = rpd + B * model.paircorr2d(r, 0., np.sqrt(2) * sdreps)

    # Add 2nd set of reps
    rpd = rpd + C * model.paircorr2d(r, 0., np.sqrt(2) * subsd)

    return rpd

popt6, pcov6 = curve_fit(
    nfoldplusrepsx2, r, histxyscale, p0=(120., 25., 10., 0.01, 50., 4., 10., 10., 10.),
    bounds=(0., [200., 50., 100., 0.1, 140., 10., 50., 50., 50.]))
plt.plot(nfoldplusrepsx2(r, *popt6))
perr6 = np.sqrt(np.diag(pcov6))
params6 = np.column_stack((popt6, perr6))
ssr6 = np.sum((nfoldplusrepsx2(r, *popt6) - histxyscale) ** 2)
# Single peak for repeated localisations gives better fits as n increases,
# because of dip between first two peaks


### Trying to get good fit to first peak
poptreps, pcovreps = curve_fit(
    repeatlocsrpdtheory, r[0:6], histxyscale[0:6])

# 2nd peak of histxy is at r = 38 nm

stats8 = ks_2samp(histxyscale, nfoldplusrepsx2(r, *popt8))


p0=(
        100., 10., 10., # diameter, sigma, amplitude
        0.1, 50., # background slope, onset 
        3., 10., # loc prec1, amp1
        10., 10. # loc prec2, amp2
        )
bounds=(
        0. ,
        [
        200., 50., 100.,
        1., 150., #1000., 1000., 1000.,# 1000.,# 1000., # 1000.,
        20., 50.,
        50., 50.
         ])

# Get AIC
mod = nfoldplusrepsx2
poptmod, pcovmod = curve_fit(
    mod, np.arange(fitlength) + 0.5, xy,
    p0=(
        100., 10., 10., # diameter, sigma, amplitude
        0.1, 50., # background slope, onset 
        3., 10., # loc prec1, amp1
        10., 10. # loc prec2, amp2
        ),
    bounds=(
        0. ,
        [
        200., 50., 100.,
        1., 150., #1000., 1000., 1000.,# 1000.,# 1000., # 1000.,
        20., 50.,
        50., 50.
         ])
    )
plt.plot(np.arange(fitlength) + 0.5, mod(np.arange(fitlength) + 0.5, *poptmod))
perrmod = np.sqrt(np.diag(pcovmod))
params = np.column_stack((poptmod, perrmod))
print params
#totint = [1.]
#intquad = integrate.quad(mod, 0, fitlength, args=tuple(poptmod))
#intstep = np.sum(mod(np.arange(0., fitlength, 0.001), *poptmod)) / 1000.
k = float(len(poptmod) + 1) # No. free parameters, including var. of residuals
                     # for least squares fit.
#aic = -2 * np.sum(np.log(mod(axpoints, *poptmod) / intstep)) + (
#            2 * k)
ssr = np.sum((mod(np.arange(fitlength) + 0.5, *poptmod) - xy) ** 2)
aic = fitlength * np.log(ssr / fitlength) + 2 * k
aiccorr = aic + 2 * k * (k + 1) / (fitlength - k - 1)
#aiccorr = aic + 2 * k * (k + 1) / (len(axpoints) - k - 1)
#print 'intquad =', intquad[0]
#print 'intstep =', intstep
print 'SSR = ', ssr
print 'AIC =', aic
print 'AICcorr =', aiccorr

# Empirical CDF
ecdf = model.generatecdf(np.sort(xypoints))
plt.plot(xypoints, ecdf)

# CDF for model
modint = nfoldplusrepsx2int
totint = integrate.quad(modint, 0, xypoints[-1], args=tuple(poptmod))[0]
cdfmod = np.zeros(len(xypoints))
cdfmod[0] = integrate.quad(
                modint, 0, xypoints[0], args=tuple(poptmod))[0] / totint
for c in range(1, len(cdfmod)):
    cdfmod[c] = cdfmod[c - 1] + integrate.quad(
            modint, xypoints[c - 1], xypoints[c], args=tuple(poptmod))[0] / totint
plt.plot(xypoints, cdfmod)

# KS statistic
D = np.max(np.abs(ecdf - cdfmod))


p0=(
        100., 10., 10., # diameter, sigma, amplitude
        0.1, 50., # background slope, onset 
        3., 10., # loc prec1, amp1
        10., 10. # loc prec2, amp2
        )
bounds=(
        0. ,
        [
        200., 50., 100.,
        1., 150., #1000., 1000., 1000.,# 1000.,# 1000., # 1000.,
        20., 50.,
        50., 50.
         ])

### Generate random samples from PDF to get distribution of K-S D-values
t = time.time()

N = 606705
#mod = linrepnoreps5fixedpeakratios
Dsamp = np.zeros(100) # Number of D's sampled for.
xmin = 0.
xmax = 200.
modint = nfoldplusrepsx2int
modfit = nfoldplusrepsx2
pdfint = integrate.quad(modint, xmin, xmax, args=tuple(poptmod))[0]
for D in range(len(Dsamp)):
    xsim = model.rejectionsample(modint, poptmod, pdfint, xmin, xmax, N)[0]
    xsim = np.sort(xsim)
    hsim = np.histogram(xsim, bins=np.arange(fitlength + 1),
                    weights=np.repeat(float(fitlength) / len(xsim), len(xsim))
                    )[0].astype(float)
    cdfsim = model.generatecdf(xsim)
    poptsim, pcovsim = curve_fit(modfit, np.arange(fitlength) + 0.5, hsim,
                           p0=p0, bounds=bounds)
    #plt.plot(np.arange(fitlength) + 0.5, mod(np.arange(fitlength) + 0.5, *poptsim))
    integral = integrate.quad(modint, 0, xsim[-1], args=tuple(poptsim))[0]
    cdfmod = np.zeros(N)
    cdfmod[0] = integrate.quad(
                modint, 0, xsim[0], args=tuple(poptsim))[0] / integral
    for c in range(1, N):
        cdfmod[c] = cdfmod[c - 1] + integrate.quad(
                modint, xsim[c - 1], xsim[c], args=tuple(poptsim))[0] / integral
    Dsamp[D] = np.max(np.abs(cdfsim - cdfmod))
    if D % 100 == 0:
        print 'Done to sample', D, 'in', time.time() - t, 's.'
print 'This all took', time.time() - t, 's.'


t = time.time()
xsim = model.rejectionsample(mod, poptmod, pdfint, xmin, xmax, N)[0]
print 'Took', time.time() - t, 's.'

def approxcdffrommodel(datapoints, mod, poptmod, xmin, xmax):
    t = time.time()
    integral = integrate.quad(mod, xmin, xmax, args=tuple(poptmod))[0]
    cdfmod = np.zeros(len(datapoints))
    cdfmod[0] = (mod(datapoints[0], *poptmod) + mod(0., *poptmod)) / 2 / integral * datapoints[0]
    for c in range(1, len(datapoints)):
        cdfmod[c] = cdfmod[c - 1] + (
            (mod(datapoints[c], *poptmod) + mod(datapoints[c - 1], *poptmod))
            / 2 / integral
            * (datapoints[c] - datapoints[c - 1])
            )
        if c % 100000 == 0:
            print 'Done', c, 'in', time.time() - t, 's.'
    return cdfmod

### PLOT MODELS

xy = generatepolypoints(8, 95.4)
h = np.histogram2d(xy[:, 0], xy[:, 1], bins=np.arange(-100., 101., 1.))[0].astype(float)
h = gaussian_filter(h, 9.5)
plt.matshow(h, cmap='gray')
    
z = np.repeat([-58.1 / 2, 58.1 / 2], len(xy))
hxz = np.histogram2d(np.append(xy[:, 0], xy[:, 0], axis=0), z,
                     bins=np.arange(-100., 101., 1.))[0].astype(float)
hxz = gaussian_filter(hxz, (9.5, 16.2))
plt.matshow(hxz, cmap='gray')


### Plotting histogram and model RPDs

plt.figure()
ax = plt.subplot(111)
xyhist = ax.hist(xypoints,
            weights=np.repeat(float(fitlength) / len(xypoints), len(xypoints)),
            bins=np.arange(float(fitlength) + 1), color='xkcd:lightblue',
            ls='None', alpha=0.5)[0]
mod7 = nfoldplusrepsx2 # After nfoldplusrepsx2 modified to be 7-fold
poptmod, pcovmod = curve_fit(
    mod, np.arange(fitlength) + 0.5, xy,
    p0=(
        100., 10., 10., # diameter, sigma, amplitude
        0.1, 50., # background slope, onset 
        3., 10., # loc prec1, amp1
        10., 10. # loc prec2, amp2
        ),
    bounds=(
        0. ,
        [
        200., 50., 100.,
        1., 150., #1000., 1000., 1000.,# 1000.,# 1000., # 1000.,
        20., 50.,
        50., 50.
         ])
    )
popt7 = poptmod
perr7 = np.sqrt(np.diag(pcovmod))
plotestci(ax, mod, popt7, perr7, reps=10000, fitlength=fitlength,
          ci=95, colour='xkcd:green')

