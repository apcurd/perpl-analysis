"""relposdensity3d.py

Functions for finding relative positions between points in 3D space and
plotting as a relative positions density function.

Will take localisations and save the relative positions between them, within a filter distance applied in 3D.

Alistair Curd
University of Leeds
30 July 2018

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""

# -*- coding: utf-8 -*-

from Tkinter import Tk
from tkFileDialog import askopenfilename
import numpy as np
import time
from scipy.ndimage.filters import gaussian_filter
import matplotlib.pyplot as plt

def loadlocs():
    """Load list of localisations: an array of localisations 
    with shape (N, x), where N is the number of localisations and x is any
    number of parameters. The first 2 or 3 columns of the file shouldbe the
    spatial coordinates of the localisations.
    
    Will ask user for a .csv or .npy file containing localisations.
    
    Returns:
        folder: Where the localisations file was contained
        infile: Containing the localisations
        xyz: The localisations.
    """  
    Tk().withdraw()    

    print 'Please select a localisations file,'
    print '.csv (or .txt with comma delimiters) or .npy,'
    print 'containing N localisations in N rows:'
    infile = askopenfilename()
    print infile    
    
    if infile[-3:] == 'npy':
        xyz = np.load(infile)
    elif infile[-3:] == 'csv' or infile[-3:] == 'txt':
        xyz = np.loadtxt(infile, delimiter=',', skiprows=1)    
    else:
        xyz = 'Ouch'
        print 'Sorry, wrong format.'
        return infile, xyz
                
    print 'This contains %i locs with %i dimensions.' % (xyz.shape[0],
                                                         xyz.shape[1])
    print ''
    print 'How many spatial dimensions shall we use (2 or 3)?'
    D = int(raw_input(
    'These should be the first 2 or 3 columns of your input file: '))
    xyz = xyz[:, 0:D]
        
    return infile, xyz
    
def getdistances(xyz, filterdist=0):
    """Store all vectors between points within a chosen distance of each other
    in all three dimensions in a numpy array. Also works for 2D.

    Args:
        xyz: numpy array of localisations with shape (N, 2 or 3),
            where N is the number of localisations.
        filterdist: distance (in all three dimensions) between points within
            which relative positions are calculated. This can be chosen by user
            input as the function runs, or by specifying when calling the
            function from a script.
        
    Returns:
        d: A numpy array of vectors of neighbours within the filter distance
            for every localisation.
        filterdist: As above, available for later use.
    """

    t0 = time.time()  # Start timing it.

    # Initialise a variable with length zero that we will reassign as the
    # array. Reassign it with the vectors to the neighbours of the first
    # localisation with near enough neighbours.

    # Set up filterdist.    
    if filterdist == 0:
        filterdist = int(raw_input(
        '\nWithin what distance do you want to identify neighbours (nm)? '))
    filterdistarray = np.array([filterdist, filterdist, filterdist])

    print '\nFinding vectors to nearby localisations:'
    
    # Add 3rd column to 2D localisations
    if xyz.shape[1] == 2:
        xyz = np.column_stack((xyz, np.zeros(xyz.shape[0])))
    
    # Initialise d (array of relative positions)  
    d = []

    # Find relative positions of near neighbours to first point.
    # Keep going through the list of points until at least one near neighbour
    # is found.
    locA = 0
    while(len(d) == 0):
        loc = xyz[locA]  # Reference localisation

        # A filter to find localisation coordinates within filterdist of loc
        # Gives np.array of True / False
        testfilter = np.logical_and(xyz > loc - filterdistarray,
                                    xyz < loc + filterdistarray)

        # Find indices of localisations within filterdist
        # in all three dimensions (all True) and select these
        chosenlocs = np.all(testfilter, axis=1)
        subxyz = xyz[chosenlocs]

        # Populate d with any near neighbours.
        # len(subxyz) == 1 would mean only the reference localisation was
        # within filterdist.
        # Remove [0,0,0]. (They can overwhelm the result.)
        # Store the vectors from reference loc to the filtered subset
        # of all locs.
        if(len(subxyz) != 1):
            d = subxyz - loc
            selectnonzeros = np.any(d != 0, axis=1)
            d = np.compress(selectnonzeros, d, axis=0)

        # Increment reference localisation
        # in search of next localisation with near neighbours.
        locA = locA + 1

    # Continue appending to the array d
    # with vectors to the near-enough neighbours of other localisations.
    for locA in range(locA, len(xyz)):
        loc = xyz[locA]
        testfilter = np.logical_and(xyz > loc - filterdistarray,
                                    xyz < loc + filterdistarray)
        chosenlocs = np.all(testfilter, axis=1)
        subxyz = xyz[chosenlocs]  
        if(len(subxyz) != 1):
            subd = subxyz - loc  #  Array of vectors to near-enough neighbours
            selectnonzeros = np.any(subd != 0, axis=1)
            subd = np.compress(selectnonzeros, subd, axis=0)
            d = np.append(d, subd, axis=0)

        # Progress message        
        if locA % 5000 == 0:
            print 'Done loc', locA, 'of', len(xyz)
            print '%i seconds so far.' % (time.time() - t0)

    print 'Found %i vectors for all localisations' % len(d)
    print 'in %i seconds.' % (time.time() - t0)

    return d, filterdist
    
def saverelpos(d, filterdist, infile):
    """Save the relative positions.
    """
    savefile = infile[0:-4] + '_PERPL-relposns_%.1ffilter.csv' % filterdist
    np.savetxt(savefile, d, delimiter=',')
    return savefile

def hist1d(nns, filterdist):
    """Plot histogram of Euclidean distances from relative positions.
    Args:
        nns: Array of 3D relative positions.
        filterdist: In getdistances, this was the distance within which
            relative positions were calculated. It is used here to set the
            maximum value for the histogram.

    Returns:
        H: histogram of Euclidean distances between localisations, from
           array of relative positions between them (calculated with
           getdistances, for instance). In 1 nm bins, ending at filterdist.    
    """
    if nns.shape[1] == 2:
        nns = np.column_stack((nns, np.zeros(nns.shape[0])))
        
    H = np.histogram(np.sqrt(
                            nns[:, 0] ** 2 + nns[:, 1] ** 2 + nns[:, 2] ** 2),
                       bins=range(filterdist))[0].astype(float)
    plt.plot(H)
    
    return H

def density3d(nns, filterdist):
    """3D density information for the neighbours.

    Vectors to neighbours are binned into 1 nm (or other consistent length
    unit) bins, centred from -filterdist to filterdist in all dimensions.

    Histogram is convolved with Gaussian kernel,
    with user input.

    Args:
        nns: Array of relative positions.
            First column of nns is vertical in FOV,
            second is horizontal,
            third is depth.
        filterdist: In getdistances, this was the distance within which
            relative positions were calculated. It is used here to set the
            extrema of the 3D histogram.

    Returns:
        convolved_hist: 3d density map of vectors to neighbours within the
            filter distance.
            First column will be vertical in FOV,
            second will be horizontal,
            third will be depth.
        blur: kernel (SD) of 3D gaussian blur applied to density histogram
        hist: Unblurred 3d density histogram, same axes.
    """
    
    blur = float(raw_input('SD of Gaussian blur to apply to plot (nm): '))
    print 'Plotting vectors in histogram.'
    edge = np.array(range(-filterdist, filterdist + 2)) - 0.5
    hist, edges = np.histogramdd(nns, bins=(edge, edge, edge))
    print 'Blurring histogram.'
    convolved_hist = gaussian_filter(hist, sigma=blur)

    # Add a bright reference point at [0,0,0]:
    convolved_hist[filterdist, filterdist, filterdist] = np.max(convolved_hist)

    return convolved_hist, blur, hist

def main():
    infile, xyz = loadlocs()
    d, filterdist = getdistances(xyz, filterdist=0)
    savefile = saverelpos(d, filterdist, infile)
    print 'Saved RPD as ' + savefile
    return

if __name__ == '__main__':
    Tk().withdraw()
    main()
    print '\nHit Enter to exit'
    raw_input()
