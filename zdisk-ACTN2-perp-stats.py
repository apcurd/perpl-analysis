# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 14:28:49 2018

@author: fbsacu
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d
import modelling_general as model
from scipy.optimize import curve_fit
from scipy.stats import kstest
from scipy.stats import ks_2samp
from scipy.stats import shapiro
from scipy import integrate
import time





#relpos = pd.read_pickle('S:/Peckham/Bioimaging/Alistair/PALM-STORM/AlexasNearestNeighbours/Actinin/acta2_NNS_aligned_6FOVs_len1229656.pkl')
#relpos = pd.read_pickle('C:/Temp/acta2_NNS_aligned_6FOVs_len1229656.pkl')

relpos = pd.read_pickle('S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2017-04-04-FOV1_nm_hor69-177_vert21-294_z0-90_qualmin0.40_maxprec5_NNS_Rot28_len2440488.pkl')
relpos = relpos.append(pd.read_pickle('S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2018-02-22-FOV1_onlyAcq1_nm_hor68-235_vert0-334_z36-76_qualmin0.40_maxprec5_NNS_Rot79_len78350.pkl'))
relpos = relpos.append(pd.read_pickle('S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2018-09-10-FOV1_nm_hor0-257_vert90-321_z25-86_qualmin0.40_maxprec5_NNS_Rot-60_len6218.pkl'))

relpos.axial = abs(relpos.axial)
        
# 2D density (1-nm bin histogram) of cell-axial/cell-tranverse relative
# positions
hist, edges = np.histogramdd(np.column_stack(
                  (relpos.transverse, relpos.axial)),
                  bins=(np.arange(0, 251, 1), np.arange(0, 251, 1)))



normhist = np.zeros(hist.shape)
for num, row in enumerate(hist):
    normhist[num] = row / (2. * num + 1.)


# Cell-transverse distance histogram
fitlength = 60 ## TODO User input for this
trans = np.sum(normhist[0:(fitlength + 1), 0:10], axis=1)
trans = trans / 2. # Remove duplicates
transsmooth = gaussian_filter1d(trans, 4.4) # 4.4 for Affimer, 4.8 for mEos2
x = np.arange(len(trans))

# Unbinned transverse relative positions
#fitlength = float(translength)
fitlength = 100
transpoints = relpos.transverse[(relpos.transverse < float(fitlength)) & (np.abs(relpos.axial) < 5)]

# Sort and remove duplicates
transpoints = np.sort(transpoints)
transpoints = transpoints[::2]

# Plot histogram, corrected for 1/r likelihood of detecting localisations
trans = plt.hist(transpoints, weights=1./transpoints, bins=np.arange(float(fitlength) + 1), color='lightblue')[0]
#trans = plt.hist(transpoints, bins=np.arange(float(fitlength) + 1), color='lightblue')[0]

# Relative positions perpendicular to cell axis, parallel to coverslip
fitlength = 150
perppoints = relpos.perpaxis2D[(np.abs(relpos.perpaxis2D) < fitlength) & 
                               (np.abs(relpos.z_nzcorr) < 5) & (relpos.axial < 5)]
perppoints = np.sort(perppoints)
perppoints = perppoints[::2]
perp = plt.hist(perppoints, bins=np.arange(float(fitlength) + 1), color='lightblue')[0]

# Mean ACTN loc.prec. was 3.4
def noslope(x, mean):
    rpd = mean + 0. * x
    return rpd

def slopeonly(x, slope):
    rpd = slope * x
    return rpd

def linfit(x, offset, slope):
    rpd = offset + slope * x
    return rpd

poptlin, pcovlin = curve_fit(linfit, x[0:90], axsmooth[0:90])
plt.plot(linfit(x[0:90], *poptlin))
perr = np.sqrt(np.diag(pcov))
params = np.column_stack((popt, perr))

statsax = ks_2samp(ax[0:100], linfit(x[0:100], *popt))
statsaxsmooth = ks_2samp(axsmooth[0:100], linfit(x[0:100], *popt))

def justreplocs(x, mean,
                   locprec, ampreplocs,
                   ):
    rpd = mean + 0. * x # Linear background
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)

    return rpd

def justreplocswithonlyslope(x, slope,
                   locprec, ampreplocs,
                   ):
    rpd = slope * x # Linear background
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)

    return rpd

def justreplocswithslope(x, mean, slope,
                   locprec, ampreplocs,
                   ):
    rpd = mean + slope * x # Linear background
    rpd = rpd + ampreplocs * model.paircorr2d(x, 0., np.sqrt(2) * locprec)

    return rpd

def tworeplocs(x, mean,
                   locprec1, ampreplocs1, locprec2, ampreplocs2
                   ):
    rpd = mean + 0. * x # Linear background
    rpd = rpd + ampreplocs1 * model.paircorr1d(x, 0., np.sqrt(2) * locprec1) 
    rpd = rpd + ampreplocs2 * model.paircorr1d(x, 0., np.sqrt(2) * locprec2) 
    return rpd

def justreplocswithampdecay(x, mean, decayamp, decaypoint,
                   locprec, ampreplocs,
                   ):
    rpd = mean - decayamp * np.exp(-x / decaypoint) # Linear background
    rpd = rpd + ampreplocs * model.paircorr2d(x, 0., np.sqrt(2) * locprec) 

    return rpd

def tworeplocswithampdecay(x, mean, decayamp, decaypoint,
                   locprec1, ampreplocs1, locprec2, ampreplocs2
                   ):
    rpd = mean - decayamp * np.exp(-x / decaypoint) # Linear background
    rpd = rpd + ampreplocs1 * model.paircorr2d(x, 0., np.sqrt(2) * locprec1) 
    rpd = rpd + ampreplocs2 * model.paircorr2d(x, 0., np.sqrt(2) * locprec2) 
    return rpd

def justonedist(x, mean, sep, broadening, amp):
    rpd = mean + 0. * x
    rpd = rpd +  amp * model.paircorr2d(x, sep, broadening)
    return rpd

def replocsplusonedist(x, mean, locprec, ampreplocs, sep, broadening, amp):
    rpd = mean + 0. * x
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    rpd = rpd +  amp * model.paircorr1d(x, sep, broadening)
    return rpd

def replocsplusonedistnobg(x, locprec, ampreplocs, sep, broadening, amp):
    rpd = ampreplocs * model.paircorr2d(x, 0., np.sqrt(2) * locprec)
    rpd = rpd +  amp * model.paircorr2d(x, sep, broadening)
    return rpd

def twodistsqlattice(x, mean, sep, broadening, amp):
    rpd = mean + 0. * x
    rpd = rpd + amp * model.paircorr2d(x, sep, broadening)
    rpd = rpd + amp * model.paircorr2d(x, sep * np.sqrt(2), broadening)
    return rpd

def replocsplustwodistsqlattice(x, mean, locprec, ampreplocs, sep, broadening, amp):
    rpd = mean + 0. * x
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    rpd = rpd + amp * model.paircorr1d(x, sep, broadening)
    rpd = rpd + amp * model.paircorr1d(x, sep * np.sqrt(2), broadening)
    return rpd

def replocsplus2sqlatticeincreasebroads(x, mean, locprec, ampreplocs, sep, amp, broadamp, broadrate):
    rpd = mean + 0. * x
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    dists = np.array([sep, np.sqrt(2) * sep])
    for d in dists:
        if (d * broadrate) < 700: # save large exponentials
            if (np.sqrt(2) * locprec + broadamp * np.exp(d * broadrate)) < d:
                rpd = rpd + amp * model.paircorr1d(x, d, np.sqrt(2) * locprec + broadamp * np.exp(d * broadrate))
            else:
                rpd = rpd + amp * model.paircorr1d(x, d, d)
        else:
            rpd = rpd + amp * model.paircorr1d(x, d, d)

    return rpd

def replocsplustwodistsqlatticewithdecay(x, mean, decayamp, decaypoint, locprec, ampreplocs, sep, broadening):
    rpd = mean - decayamp * np.exp(-x / decaypoint)
    rpd = rpd + ampreplocs * model.paircorr2d(x, 0., np.sqrt(2) * locprec)
    rpd = rpd + np.sqrt(2 * np.pi) * broadening * model.paircorr2d(x, sep, broadening) * 2 * decayamp * np.exp(-sep / decaypoint)
    rpd = rpd + np.sqrt(2 * np.pi) * broadening * model.paircorr2d(x, sep * np.sqrt(2), broadening) * 2 * decayamp * np.exp(-sep * np.sqrt(2) / decaypoint)
    return rpd

def fivesqlattice(x, mean, sep, broadening, amp):
    rpd = mean + 0. * x

    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10),
                 sep * np.sqrt(13),
                 sep * np.sqrt(13),
                 sep * 4.,
                 sep * np.sqrt(18),
                 sep * np.sqrt(17),
                 sep * np.sqrt(17),
                 sep * np.sqrt(20),
                 sep * np.sqrt(20),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25)])
    for d in dists:
        rpd = rpd + amp * model.paircorr2d(x, d, broadening)
    
    return rpd


def threesqlatticewithdecay(x, mean, decayamp, decaypoint, locprec, ampreplocs, sep, broadening):
    rpd = mean - decayamp * np.exp(-x / decaypoint)
    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10)])
    for d in dists:
        rpd = rpd + np.sqrt(2 * np.pi) * broadening * model.paircorr2d(x, d, broadening) * 2 * decayamp * np.exp(-d / decaypoint)
    
    return rpd

def replocsplus3rsqlattice(x, mean, locprec, ampreplocs, sep, broadening, amp):
    rpd = mean + 0. * x
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10)])
    for d in dists:
        rpd = rpd + amp * model.paircorr1d(x, d, broadening)
    
    return rpd

def replocsplus3sqlatticeincreasebroads(x, mean, locprec, ampreplocs, sep, amp, broadamp, broadrate):
    rpd = mean + 0. * x
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10)])
    
    for d in dists:
        if (d * broadrate) < 700: # save large exponentials
            if (np.sqrt(2) * locprec + broadamp * np.exp(d * broadrate)) < d:
                rpd = rpd + amp * model.paircorr1d(x, d, np.sqrt(2) * locprec + broadamp * np.exp(d * broadrate))
            else:
                rpd = rpd + amp * model.paircorr1d(x, d, d)
        else:
            rpd = rpd + amp * model.paircorr1d(x, d, d)
    
    return rpd

def replocsplus3rsqlatticewithdecay(x, mean, decayamp, decaypoint, locprec, ampreplocs, sep, broadening, amp):
    rpd = mean - decayamp * np.exp(-x / decaypoint)
    rpd = rpd + ampreplocs * model.paircorr2d(x, 0., np.sqrt(2) * locprec)
    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10)])
    for d in dists:
        rpd = rpd + amp * model.paircorr2d(x, d, broadening) * 2 * decayamp * np.exp(d / decaypoint)
    
    return rpd

def replocsplus5sqlattice(x, mean, locprec, ampreplocs, sep, broadening, amp):
    rpd = mean + 0. * x
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10),
                 sep * np.sqrt(13),
                 sep * np.sqrt(13),
                 sep * 4.,
                 sep * np.sqrt(18),
                 sep * np.sqrt(17),
                 sep * np.sqrt(17),
                 sep * np.sqrt(20),
                 sep * np.sqrt(20),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25)])
    for d in dists:
        rpd = rpd + amp * model.paircorr1d(x, d, broadening)
    
    return rpd

def replocsplus4sqlatticeincreasebroads(x, mean, locprec, ampreplocs, sep, amp, broadamp, broadrate):
    rpd = mean + 0. * x
    rpd = rpd + ampreplocs * model.paircorr2d(x, 0., np.sqrt(2) * locprec)
    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10),
                 sep * np.sqrt(13),
                 sep * np.sqrt(13),
                 sep * 4.])
    
    for d in dists:
        if (d * broadrate) < 700: # save large exponentials
            if (np.sqrt(2) * locprec + broadamp * np.exp(d * broadrate)) < d:
                rpd = rpd + amp * model.paircorr2d(x, d, np.sqrt(2) * locprec + broadamp * np.exp(d * broadrate))
            else:
                rpd = rpd + amp * model.paircorr2d(x, d, d)
        else:
            rpd = rpd + amp * model.paircorr2d(x, d, d)
    
    return rpd

def replocsplus5sqlatticeincreasebroads(x, mean, locprec, ampreplocs, sep, amp, broadamp, broadrate):
    rpd = mean + 0. * x
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10),
                 sep * np.sqrt(13),
                 sep * np.sqrt(13),
                 sep * 4.,
                 sep * np.sqrt(18),
                 sep * np.sqrt(17),
                 sep * np.sqrt(17),
                 sep * np.sqrt(20),
                 sep * np.sqrt(20),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25)])
    
    for d in dists:
        if (d * broadrate) < 700: # save large exponentials
            if (np.sqrt(2) * locprec + broadamp * np.exp(d * broadrate)) < d:
                rpd = rpd + amp * model.paircorr1d(x, d, np.sqrt(2) * locprec + broadamp * np.exp(d * broadrate))
            else:
                rpd = rpd + amp * model.paircorr1d(x, d, d)
        else:
            rpd = rpd + amp * model.paircorr1d(x, d, d)
    
    return rpd


def replocsplus5sqlatticenobg(x, locprec, ampreplocs, sep, broadening, amp):
    rpd = ampreplocs * model.paircorr2d(x, 0., np.sqrt(2) * locprec)
    dists = np.array(
                [sep,
                 sep * np.sqrt(2),
                 sep * 2.,
                 sep * np.sqrt(5),
                 sep * np.sqrt(5),
                 sep * np.sqrt(8),
                 sep * 3.,
                 sep * np.sqrt(10),
                 sep * np.sqrt(10),
                 sep * np.sqrt(13),
                 sep * np.sqrt(13),
                 sep * 4.,
                 sep * np.sqrt(18),
                 sep * np.sqrt(17),
                 sep * np.sqrt(17),
                 sep * np.sqrt(20),
                 sep * np.sqrt(20),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25),
                 sep * np.sqrt(25)])
    for d in dists:
        rpd = rpd + amp * model.paircorr2d(x, d, broadening)
    
    return rpd


def slopeplusreplocs(x, slope, locprec, ampreplocs):
    rpd = slope * x
    rpd = rpd + ampreplocs * model.paircorr2d(x, 0., np.sqrt(2) * locprec)
    return rpd

def slopeplusonedist(x, slope, sep, broadening, amp):
    rpd = slope * x
    rpd = rpd +  amp * model.paircorr2d(x, sep, broadening)
    return rpd

popt, pcov = curve_fit(
    linrepplusreps5, np.arange(151.), ax[0:151],
    p0=(20., 3.4,
        1., 1., 1., 1., 1.,# 1., # 1.,
        3., 1.,
        -0.2, 20.
        ),
    bounds=([0., 0.,
             0., 0., 0., 0., 0.,# 0., # 0.,
             0., 0.,
             -100., 0.
             ] ,
        [100., 10.,
         1000., 1000., 1000., 1000.,  1000.,# 1000., # 1000.,
         20., 1000.,
         100., 100.
         ])
    )
    
plt.plot(linrepplusreps(x[0:150], *popt))
perr = np.sqrt(np.diag(pcov))
params = np.column_stack((popt, perr))
ssr = np.sum((linrepplusreps(x, *popt) - ax) ** 2)

statsax = ks_2samp(ax[0:100], linrepplusreps(x[0:100], *popt))
statsaxsmooth = ks_2samp(axsmooth[0:100], linrepplusreps(x[0:100], *popt))

amps = popt[2:7]
broadening = popt[1]
rep = popt[0]
locprec, ampreplocs, bgslope, bgoffset = popt[7:11]

background = bgoffset + bgslope * x[0:100]

peaks = np.zeros(100)
for i, amp in enumerate(amps):
    peaks = peaks + amp * model.gauss1d(x[0:100], (i + 1) * rep, broadening)
    
locpreccurve = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))



# Get bootstrap samples of all axial separations when transverse
# separation < 10 nm
reps = 100000
boots = model.bootstrap(relpos.axial[relpos.transverse < 10.], reps)


# Calculate bootstrap smoothed profiles
axsmoothb = np.zeros((reps, axlength))
axb = np.zeros((reps, axlength))
for i in range(reps):
    hist, edges = np.histogram(boots[i], bins=(np.arange(0, axlength + 1, 1)))
    axb[i] = hist / 2. # Remove duplicates
    axsmoothb[i] = gaussian_filter1d(axb[i], 4.8)
    # plt.plot(axsmoothb)
    
axsmsort = np.sort(axsmoothb, axis=0)
plt.plot(axsmooth, '-', axsmsort[reps/20], '-', axsmsort[reps * 19/20], '-')

stats = np.zeros((reps, 2))

for i in range(reps):
    stats[i] = ks_2samp(axsmoothb[i, 0:100], linrepplusreps(x[0:100], *popt))
    
float(len(stats[stats[:, 1] > 0.05])) / reps
np.mean(stats[:, 1], axis=0)

## Sample model parameters
reps = 100000
poptsamples = np.zeros((reps, len(popt)))
modsamples = np.zeros((reps, fitlength))

for i in range(reps):
    for p in range(len(popt)):
        poptsamples[i, p] = np.random.normal(loc=popt[p], scale=perr[p])
    modsamples[i] = linrepplusreps5(x[0:fitlength], *poptsamples[i])
modsort = np.sort(modsamples, axis=0)

plt.plot(linrepplusreps5(x[0:90], *popt), '-',
         linrepplusreps5(x[0:90], *poptsort[reps/20]), '-',
         linrepplusreps5(x[0:90], *poptsort[reps * 19/20]), '-'
         )

# Calculate AIC
# example
# fitlength set when finding axpoints above

mod = justreplocswithampdecay
poptmod, pcovmod = curve_fit(
    mod, np.arange(fitlength) + 0.5, perp,
    p0=(10., # mean
        1., 0.1, # decay amp & rate or distance
        10., 100., # loc prec & amp
        20., 10., 100., # lattice constant, broadening, amplitude
        ),
    bounds=([
        0.,
        0., 0.,
        0., 0.,
        0., 0., 0.,    
             ] ,
        [
        100.,
        100., 5.,
        50., 1000.,
        50., 50., 1000.,
         ])
    )
plt.plot(mod(np.arange(fitlength) + 0.5, *poptmod))
perrmod = np.sqrt(np.diag(pcovmod))
params = np.column_stack((poptmod, perrmod))
print params
#totint = [1.]
#intquad = integrate.quad(mod, 0, fitlength, args=tuple(poptmod))
#intstep = np.sum(mod(np.arange(0., fitlength, 0.001), *poptmod)) / 1000.
k = float(len(poptmod) + 1) # No. free parameters, including var. of residuals
                     # for least squares fit.
#aic = -2 * np.sum(np.log(mod(transpoints, *poptmod) / intstep)) + (
#            2 * k)
ssr = np.sum((mod(np.arange(fitlength) + 0.5, *poptmod) - perp[0:int(fitlength)]) ** 2)
aic = fitlength * np.log(ssr / fitlength) + 2 * k
aiccorr = aic + 2 * k * (k + 1) / (fitlength - k - 1)
#aiccorr = aic + 2 * k * (k + 1) / (len(perppoints) - k - 1)
#print 'intquad =', intquad[0]
#print 'intstep =', intstep
print 'SSR = ', ssr
print 'AIC =', aic
print 'AICcorr =', aiccorr

# For normalisation of PDF
totint = integrate.quad(linrepplusreps6, 0, axpoints[-1], args=tuple(popt))

k = 13 # Number of free parameters.
       # Includes + 1 for var. of residuals in a least-squares fit model.

#Calculate AIC
aicp6 = -2 * np.sum(np.log(linrepplusreps6(axpoints, *popt) / integralp6)) + (
            2 * k)

#Calculate AIC_C
aiccorr = aic + 2 * k * (k + 1) / (n - k - 1)

# Empirical CDF
ecdf = model.generatecdf(np.sort(axpoints))
plt.plot(axpoints, ecdf)

# Linear CDF
modcdf = model.lincdf(axpoints, 0., axpoints[-1], *poptmod)

# CDF for model
totint = integrate.quad(linrepplusreps5, 0, axpoints[-1], args=tuple(popt))
cdfp5 = np.zeros(len(axpoints))
for c in range(len(cdfp5)):
    cdfp5[c] = integrate.quad(
            linrepplusreps5, 0, axpoints[c], args=tuple(popt))[0] / totint[0]
plt.plot(axpoints, cdfp5)

# KS statistic
D = np.max(np.abs(ecdf - cdfp5))

# p > 0.2 critical value
crit = 1.07 / np.sqrt(len(axpoints))

# Test line for git


## BOOTSTRAP AND KS-TESTS
reps = 40000

boots = model.bootstrap(axpoints, reps)

ecdfboots = np.zeros(boots.shape)
for i in range(ecdfboots.shape[0]):
    ecdfboots[i] = model.generatecdf(np.sort(boots[i]))

## THIS TAKES 37 hours WITH 40000 REPS!!
t = time.time()
cdfp5boot = np.zeros(boots.shape)
for samp in range(cdfp5boot.shape[0]):
    totint = integrate.quad(linrepplusreps5, 0, boots[samp, -1], args=tuple(popt))
    for point in range(cdfp5boot.shape[1]):
        cdfp5boot[samp, point] = integrate.quad(
            linrepplusreps5, 0, boots[samp, point], args=tuple(popt))[0] / totint[0]
print time.time() - t, 's'

Dboots = np.zeros(boots.shape[0])
for i in range(100):
    Dboots[i] = np.max(np.abs(ecdfboots[i] - cdfp5boot[i]))
    
crit02 = 1.07 / np.sqrt(len(axpoints))
len(Dboots[0:100][Dboots[0:100] < crit02]) # = 69

crit005 = 1.36 / np.sqrt(len(axpoints))
len(Dboots[0:100][Dboots[0:100] < crit005]) # = 92

# Generate random samples from PDF to get distribution of K-S D-values
t = time.time()

N = 937
Dsamp = np.zeros(100) # Number of D's sampled for.
for D in range(len(Dsamp)):
    xsim = model.rejectionsample(linrepplusreps5, poptmod, 0., 100., N)[0]
    xsim = np.sort(xsim)
    hsim = np.histogram(xsim, bins=np.arange(101.))[0].astype(float)
    cdfsim = model.generatecdf(xsim)
    popt, pcov = curve_fit(linrepplusreps5, np.arange(100.), hsim,
                           p0=(20., 3.4,
        1., 1., 1., 1., 1.,# 1., # 1.,
        3., 1.,
        -0.2, 20.
        ),
        bounds=([0., 0.,
             0., 0., 0., 0., 0.,# 0., # 0.,
             0., 0.,
             -100., 0.
             ] ,
        [100., 10.,
         1000., 1000., 1000., 1000.,  1000.,# 1000., # 1000.,
         20., 1000.,
         100., 100.
         ])
    )
    integral = integrate.quad(linrepplusreps5, 0, xsim[-1], args=tuple(popt))[0]
    cdfp5 = np.zeros(N)
    cdfp5[0] = integrate.quad(
                linrepplusreps5, 0, xsim[0], args=tuple(popt))[0] / integral
    for c in range(1, N):
        cdfp5[c] = cdfp5[c - 1] + integrate.quad(
                linrepplusreps5, xsim[c - 1], xsim[c], args=tuple(popt))[0] / integral
    Dsamp[D] = np.max(np.abs(cdfsim - cdfp5))

print time.time() - t, 's.'

