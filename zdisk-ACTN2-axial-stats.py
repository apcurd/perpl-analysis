# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 14:28:49 2018

@author: fbsacu
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d
import modelling_general as model
from scipy.optimize import curve_fit
from scipy import stats
from scipy.stats import kstest
from scipy.stats import ks_2samp
from scipy.stats import shapiro
from scipy import integrate
import time





#relpos = pd.read_pickle('S:/Peckham/Bioimaging/Alistair/PALM-STORM/AlexasNearestNeighbours/Actinin/acta2_NNS_aligned_6FOVs_len1229656.pkl')
#relpos = pd.read_pickle('C:/Temp/acta2_NNS_aligned_6FOVs_len1229656.pkl')

#relpos = pd.read_pickle('S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2017-04-04-FOV1_nm_hor69-177_vert21-294_z0-90_qualmin0.40_maxprec5_NNS_Rot28_len2440488.pkl')
#relpos = relpos.append(pd.read_pickle('S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2018-02-22-FOV1_onlyAcq1_nm_hor68-235_vert0-334_z36-76_qualmin0.40_maxprec5_NNS_Rot79_len78350.pkl'))
#relpos = relpos.append(pd.read_pickle('S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2018-09-10-FOV1_nm_hor0-257_vert90-321_z25-86_qualmin0.40_maxprec5_NNS_Rot-60_len6218.pkl'))

#inpickles = ['S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2018-02-22-FOV1_onlyAcq1_nm_hor68-235_vert0-334_z36-76_qualmin0.40_maxprec3_NNS_Rot79_len89110.pkl',
# 'S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2017-04-04-FOV1_nm_hor69-177_vert21-294_z0-90_qualmin0.40_maxprec3_NNS_Rot28_len2034450.pkl',
# 'S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2018-09-10-FOV1_nm_hor0-257_vert90-321_z25-86_qualmin0.40_maxprec3_NNS_Rot-60_len3522.pkl']

inpickles = ['S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2017-04-04-FOV1_nm_hor69-177_vert21-294_z0-90_qualmin0.40_maxprec5_NNS_Rot28_len2440488.pkl',
             'S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2018-09-10-FOV1_nm_hor0-257_vert90-321_z25-86_qualmin0.40_maxprec5_NNS_Rot-60_len6218.pkl',
             'S:/Peckham/Bioimaging2/Brendan STORM/20180910 PERPL data/actinin/actinin-Aff9-2018-02-22-FOV1_onlyAcq1_nm_hor68-235_vert0-334_z36-76_qualmin0.40_maxprec5_NNS_Rot79_len78350.pkl']

relpos = pd.read_pickle(inpickles[0])
relpos = relpos.append(pd.read_pickle(inpickles[1]))
relpos = relpos.append(pd.read_pickle(inpickles[2]))


relpos.axial = abs(relpos.axial)
        
# 2D density (1-nm bin histogram) of cell-axial/cell-tranverse relative
# positions
#hist, edges = np.histogramdd(np.column_stack(
#                  (relpos.transverse, relpos.axial)),
#                  bins=(np.arange(0, 251, 1), np.arange(0, 251, 1)))



# Cell-axial distance histogram
#axlength = 70 ## TODO User input for this
#ax = np.sum(hist[0:10, 0:(axlength + 1)], axis=0)
#ax = ax / 2. # Remove duplicates
#axsmooth = gaussian_filter1d(ax, 4.4) # 4.4 for Affimer, 4.8 for mEos2
#x = np.arange(len(ax))

# Unbinned axial relative positions
fitlength = 100.
axpoints = relpos.axial[(relpos.axial < fitlength) & (relpos.transverse < 10)]

# Using extra points to allow smoothing upto fitlength
fitlength = 100.
smoothing = 4.8 # for mEos2
axpoints = relpos.axial[(relpos.axial < (fitlength + smoothing * 3)) &
                        (relpos.transverse < 10)] # so points beyond those not
                #included would only have 1% effect on the values at fitlength



# Sort and remove duplicates
axpoints = np.sort(axpoints)
axpoints = axpoints[::2]

# Plot histogram
ax = plt.hist(axpoints, bins=np.arange(fitlength + 1), color='lightblue')[0]
# axsmooth = gaussian_filter1d(ax, smoothing) # Smoothed histogram
# kde method
kernel_scalar = 4.8 / np.std(axpoints)
kernel = stats.gaussian_kde(axpoints, bw_method=kernel_scalar)
x = np.arange(np.round(smoothing * 3) + 0.5, fitlength, 1.)
axsmooth = kernel(x) * len(axpoints) # Mulitply because normalised
                                                # as PDF
# Mean ACTN loc.prec. was 3.4
def noslope(x, mean):
    rpd = mean + 0. * x
    return rpd

def linfit(x, slope, offset):
    rpd = offset + slope * x
    return rpd

poptlin, pcovlin = curve_fit(linfit, np.arange(fitlength) + 0.5, ax)
plt.plot(np.arange(fitlength) + 0.5, linfit(np.arange(fitlength) + 0.5, *poptlin))
perr = np.sqrt(np.diag(pcov))
params = np.column_stack((popt, perr))

statsax = ks_2samp(ax[0:100], linfit(x[0:100], *popt))
statsaxsmooth = ks_2samp(axsmooth[0:100], linfit(x[0:100], *popt))

def justreplocs(x,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background

    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    
    return rpd

def onepeakplusreps(x, rep, broadening,
                   a,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    amps = [a]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    reps = ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def onepeaknoreps(x, rep, broadening,
                   a,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    amps = [a]
    for i, amp in enumerate(amps):
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    return rpd

def linrepplusreps(x, rep, broadening,
                   a, b, c, d, # e, f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d]#, e, f]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps3(x, rep, broadening,
                   a, b, c,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    amps = [a, b, c]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec) # rep locs
    
    return rpd

def offsetlinrepplusreps3flatbg(x, rep, broadening,
                   a, b, c,
                   peakoffset,
                   locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = bgoffset + 0. * x # Linear background
    amp = [a, b, c]
    for i in range(3):
        rpd = rpd +  amp[i] * model.paircorr1d(
                                    x, peakoffset + (i + 1) * rep, broadening)    
    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)

    return rpd

def linrepplusreps3fixedpeaks(x, rep, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(3):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps3fixedpeaksflatbg(x, rep, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = bgoffset + 0. * x # Linear background
    for i in range(3):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec) # rep locs
    return rpd

def linrepplusreps3fixedpeakratio(x, rep, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(3):
        rpd = rpd + (1. - i / 3.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec) # rep locs

    return rpd

def linrepplusreps3fixedpeakamp(x, rep, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(3):
        rpd = rpd +  amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def offsetlinrepplusreps3fixedpeakampflatbg(x, rep, broadening,
                   amp,
                   peakoffset,
                   locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = bgoffset + 0. * x # Linear background
    for i in range(3):
        rpd = rpd +  amp * model.paircorr1d(
                                    x, peakoffset + (i + 1) * rep, broadening)    
    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)

    return rpd

def offsetlinrep3fixedpeakamp(x, rep, broadening,
                   amp,
                   peakoffset,
                   bgoffset
                   ):
    """Offset peak plus 3 more at repeat distance.
    """
    rpd = bgoffset + 0. * x # Linear background
    for i in range(4):
        rpd = rpd +  amp * model.paircorr1d(
                                    x, peakoffset + i * rep, broadening)    
    
    #reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec ** 2))
    #rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def offsetlinrep3fixedpeakampnobg(x, rep, broadening,
                   amp,
                   peakoffset
                   ):
    """Offset peak plus 3 more at repeat distance.
    """
    rpd = 0. * x # Linear background
    for i in range(4):
        rpd = rpd +  amp * model.paircorr1d(
                                    x, peakoffset + i * rep, broadening)    
    
    #reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec ** 2))
    #rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepbroadsplusreps3fixedpeakratio(x, rep,
                                  b1, b2, b3,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    b = [b1, b2, b3]
    for i in range(3):
        rpd = rpd + (1. - i / 3.) * amp * model.paircorr1d(x, (i + 1) * rep, b[i])    
    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrep19plusreps3fixedpeakratio(x, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    rep = 19.2
    for i in range(3):
        rpd = rpd + (1. - i / 3.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepnoreps3fixedpeakratio(x, rep, broadening,
                   amp,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(3):
        rpd = rpd + (1. - i / 3.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrep19noreps3fixedpeakratio(x, broadening,
                   amp,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    rep = 19.2
    for i in range(3):
        rpd = rpd + (1. - i / 3.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepnoreps3fixedpeaks(x, rep, broadening,
                   amp,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(3):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
 
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps4(x, rep, broadening,
                   a, b, c, d,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    amps = [a, b, c, d]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)

    return rpd

def linrepplusreps4fixedpeakratio(x, rep, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(4):
        rpd = rpd + (1. - i / 4.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)
    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)

    return rpd

def linrepplusreps4fixedpeakrationobg(x, rep, broadening,
                   amp,
                   locprec, ampreplocs,
                   ):
    rpd = 0. * x # Linear background
    for i in range(4):
        rpd = rpd + (1. - i / 4.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)
    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec)

    return rpd

def linrepbroadsplusreps4fixedpeakratio(x, rep,
                    b1, b2, b3, b4,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    b = [b1, b2, b3, b4]
    for i in range(4):
        rpd = rpd + (1. - i / 4.) * amp * model.paircorr1d(x, (i + 1) * rep, b[i])
    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps

    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrep19plusreps4fixedpeakratio(x, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    rep = 19.2
    for i in range(4):
        rpd = rpd + (1. - i / 4.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)
    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps

    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepnoreps4(x, rep, broadening,
                   a, b, c, d,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepnoreps4fixedpeaks(x, rep, broadening,
                   amp,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(4):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepnoreps4fixedpeakratio(x, rep, broadening,
                   amp,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(4):
        rpd = rpd + (1. - i / 4.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrep19noreps4fixedpeakratio(x, broadening,
                   amp,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    rep = 19.2
    for i in range(4):
        rpd = rpd + (1. - i / 4.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps5(x, rep, broadening,
                   a, b, c, d, e,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec) # rep locs

    return rpd

def linrep5(x, rep, broadening,
                   a, b, c, d, e,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    return rpd


def randplusreps5(x, broadening,
                   xa, xb, xc, xd, xe,
                   a, b, c, d, e,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    xs = [xa, xb, xc, xd, xe]
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, xs[i], broadening)    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps5fixedpeak(x, rep, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(5):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps5fixedpeakratio(x, rep, broadening,
                   amp,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(5):
        rpd = rpd + (1. - i / 5.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    rpd = rpd + ampreplocs * model.paircorr1d(x, 0., np.sqrt(2) * locprec) # rep locs

    return rpd


def linrepnoreps5(x, rep, broadening,
                   a, b, c, d, e,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepnoreps5fixedpeak(x, rep, broadening,
                   amp,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(5):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepnoreps5fixedpeakratios(x, rep, broadening,
                   amp,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    for i in range(5):
        rpd = rpd + (1. - i / 5.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def nobg5fixedpeakratios(x, rep, broadening,
                   amp
                   ):
    rpd = x - x # Linear background
    for i in range(5):
        rpd = rpd + (1. - i / 5.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def nobg5fixedpeakratiosplusreps(x, rep, broadening,
                   amp,
                   locprec, ampreplocs
                   ):
    rpd = x - x # Linear background
    for i in range(5):
        rpd = rpd + (1. - i / 5.) * amp * model.paircorr1d(x, (i + 1) * rep, broadening)    

    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps    
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps6(x, rep, broadening,
                   a, b, c, d, e, f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepnoreps6(x, rep, broadening,
                   a, b, c, d, e, f,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background

    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def randplusreps6(x, broadening,
                   xa, xb, xc, xd, xe, xf,
                   a, b, c, d, e, f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = bgoffset + bgslope * x # Linear background
    xs = [xa, xb, xc, xd, xe, xf]
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        rpd = rpd + amp * model.paircorr1d(x, xs[i], broadening)    
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

popt, pcov = curve_fit(
    linrepplusreps5, np.arange(151.), ax[0:151],
    p0=(20., 3.4,
        1., 1., 1., 1., 1.,# 1., # 1.,
        3., 1.,
        -0.2, 20.
        ),
    bounds=([0., 0.,
             0., 0., 0., 0., 0.,# 0., # 0.,
             0., 0.,
             -100., 0.
             ] ,
        [100., 10.,
         1000., 1000., 1000., 1000.,  1000.,# 1000., # 1000.,
         20., 1000.,
         100., 100.
         ])
    )
    
plt.plot(linrepplusreps(x[0:150], *popt))
perr = np.sqrt(np.diag(pcov))
params = np.column_stack((popt, perr))
ssr = np.sum((linrepplusreps(x, *popt) - ax) ** 2)

statsax = ks_2samp(ax[0:100], linrepplusreps(x[0:100], *popt))
statsaxsmooth = ks_2samp(axsmooth[0:100], linrepplusreps(x[0:100], *popt))

amps = popt[2:7]
broadening = popt[1]
rep = popt[0]
locprec, ampreplocs, bgslope, bgoffset = popt[7:11]

background = bgoffset + bgslope * x[0:100]

peaks = np.zeros(100)
for i, amp in enumerate(amps):
    peaks = peaks + amp * model.gauss1d(x[0:100], (i + 1) * rep, broadening)
    
locpreccurve = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))



# Get bootstrap samples of all axial separations when transverse
# separation < 10 nm
reps = 100000
boots = model.bootstrap(axpoints, reps)

# Fit samples # Watch, since the samples that don't converge stop the loop.
poptsamples = np.zeros((reps, len(popt)))
for i in range(reps):
    H = np.histogram(boots[i], bins=np.arange(fitlength + 1))[0]
    poptsamples[i], pcov = curve_fit(
    mod, np.arange(fitlength) + 0.5, H,
    p0=(poptmod), # estimate from experimental data
    bounds=([
        0., 0.,
        0., #0., 0., 0.,# 0.,# 0., # 0.,
        0., .0,
        -100., 0.
             ] ,
        [
        100., 10.,
        1000., #1000., 1000., 1000.,# 1000.,# 1000., # 1000.,
        20., 1000.,
        100., 100.
         ])
    )
    
# Some of these will not converge; get rid of them
poptsamples = poptsamples[poptsamples[:, 0] != 0.]    

# Generate model RPDs for the bootstrap-sampled fits    
modsamples = np.zeros((len(poptsamples), int(fitlength)))
for i in range(len(poptsamples)):
    modsamples[i] = mod(np.arange(fitlength) + 0.5, *poptsamples[i])
modsort = np.sort(modsamples, axis=0)

# Plot CIs, e.g.:
plt.plot(np.arange(fitlength) + 0.5, modsort[int(round(len(modsort) / 100 * 2.5))])
plt.plot(np.arange(fitlength) + 0.5, modsort[int(round(len(modsort) / 100 * 97.5))])



# Calculate bootstrap smoothed profiles
axsmoothb = np.zeros((reps, axlength))
axb = np.zeros((reps, axlength))
for i in range(reps):
    hist, edges = np.histogram(boots[i], bins=(np.arange(0, axlength + 1, 1)))
    axb[i] = hist / 2. # Remove duplicates
    axsmoothb[i] = gaussian_filter1d(axb[i], 4.8)
    # plt.plot(axsmoothb)
    
axsmsort = np.sort(axsmoothb, axis=0)
plt.plot(axsmooth, '-', axsmsort[reps/20], '-', axsmsort[reps * 19/20], '-')

stats = np.zeros((reps, 2))

for i in range(reps):
    stats[i] = ks_2samp(axsmoothb[i, 0:100], linrepplusreps(x[0:100], *popt))
    
float(len(stats[stats[:, 1] > 0.05])) / reps
np.mean(stats[:, 1], axis=0)

## Sample model parameters instead - careful - this ignores covariance between parameters
reps = 100000
poptsamples = np.zeros((reps, len(popt)))
modsamples = np.zeros((reps, fitlength))

for i in range(reps):
    for p in range(len(popt)):
        poptsamples[i, p] = np.random.normal(loc=popt[p], scale=perr[p])
    modsamples[i] = linrepplusreps5(x[0:fitlength], *poptsamples[i])
modsort = np.sort(modsamples, axis=0)

plt.plot(linrepplusreps5(x[0:90], *popt), '-',
         linrepplusreps5(x[0:90], *poptsort[reps/20]), '-',
         linrepplusreps5(x[0:90], *poptsort[reps * 19/20]), '-'
         )

# Calculate AIC
# example
# fitlength set when finding axpoints above

mod = linrepplusreps5fixedpeakratio
poptmod, pcovmod = curve_fit(
    mod, np.arange(fitlength) + 0.5, ax,
    p0=(
        20., 5., # rep, broadening
        1., #1., 1., 1.,# 1.,# 1., # 1., # amps
        3., 1., # loc prec, amp
        -0.2, 20. # linear slope, offset
        ),
    bounds=([
        0., 0.,
        0., #0., 0., 0.,# 0.,# 0., # 0.,
        0., .0,
        -100., 0.
             ] ,
        [
        100., 10.,
        1000., #1000., 1000., 1000.,# 1000.,# 1000., # 1000.,
        20., 1000.,
        100., 100.
         ])
    )
plt.plot(np.arange(fitlength) + 0.5, mod(np.arange(fitlength) + 0.5, *poptmod))
perrmod = np.sqrt(np.diag(pcovmod))
params = np.column_stack((poptmod, perrmod))
print params
#totint = [1.]
#intquad = integrate.quad(mod, 0, fitlength, args=tuple(poptmod))
#intstep = np.sum(mod(np.arange(0., fitlength, 0.001), *poptmod)) / 1000.
k = float(len(poptmod) + 1) # No. free parameters, including var. of residuals
                     # for least squares fit.
#aic = -2 * np.sum(np.log(mod(axpoints, *poptmod) / intstep)) + (
#            2 * k)
ssr = np.sum((mod(np.arange(fitlength) + 0.5, *poptmod) - ax) ** 2)
aic = fitlength * np.log(ssr / fitlength) + 2 * k
aiccorr = aic + 2 * k * (k + 1) / (fitlength - k - 1)
#aiccorr = aic + 2 * k * (k + 1) / (len(axpoints) - k - 1)
#print 'intquad =', intquad[0]
#print 'intstep =', intstep
print 'SSR = ', ssr
print 'AIC =', aic
print 'AICcorr =', aiccorr

# Version with smoothed/kde histogram
mod = linfit
poptmod, pcovmod = curve_fit(
    mod, x, axsmooth,
    p0=(
        #20., 5., # rep, broadening
        #1., 1., 1., 1., 1.,# 1., # 1., # amps
        #3., 1., # loc prec, amp
        -0.2, 20. # linear slope, offset
        ),
    bounds=([
        #0., 0.,
        #0., 0., 0., 0., 0.,# 0., # 0.,
        #0., .0,
        -100., 0.
             ] ,
        [
        #100., 10.,
        #1000., 1000., 1000., 1000., 1000.,# 1000., # 1000.,
        #20., 1000.,
        100., 100.
         ])
    )
plt.plot(x, mod(x, *poptmod))
perrmod = np.sqrt(np.diag(pcovmod))
params = np.column_stack((poptmod, perrmod))
print params
#totint = [1.]
#intquad = integrate.quad(mod, 0, fitlength, args=tuple(poptmod))
#intstep = np.sum(mod(np.arange(0., fitlength, 0.001), *poptmod)) / 1000.
k = float(len(poptmod) + 1) # No. free parameters, including var. of residuals
                     # for least squares fit.
#aic = -2 * np.sum(np.log(mod(axpoints, *poptmod) / intstep)) + (
#            2 * k)
ssr = np.sum((mod(x, *poptmod) - axsmooth) ** 2)
aic = len(x) * np.log(ssr / len(x)) + 2 * k
aiccorr = aic + 2 * k * (k + 1) / (len(x) - k - 1)
#aiccorr = aic + 2 * k * (k + 1) / (len(axpoints) - k - 1)
#print 'intquad =', intquad[0]
#print 'intstep =', intstep
print 'SSR = ', ssr
print 'AIC =', aic
print 'AICcorr =', aiccorr

# For normalisation of PDF
totint = integrate.quad(linrepplusreps6, 0, axpoints[-1], args=tuple(popt))

k = 13 # Number of free parameters.
       # Includes + 1 for var. of residuals in a least-squares fit model.

#Calculate AIC
aicp6 = -2 * np.sum(np.log(linrepplusreps6(axpoints, *popt) / integralp6)) + (
            2 * k)

#Calculate AIC_C
aiccorr = aic + 2 * k * (k + 1) / (n - k - 1)

# Empirical CDF
ecdf = model.generatecdf(np.sort(axpoints))
plt.plot(axpoints, ecdf)

# Linear CDF
modcdf = model.lincdf(axpoints, 0., axpoints[-1], *poptmod)

# CDF for model
totint = integrate.quad(mod, 0, axpoints[-1], args=tuple(poptmod))
cdfmod = np.zeros(len(axpoints))
for c in range(len(cdfmod)):
    cdfmod[c] = integrate.quad(
            mod, 0, axpoints[c], args=tuple(poptmod))[0] / totint[0]
plt.plot(axpoints, cdfmod)

# KS statistic
D = np.max(np.abs(ecdf - cdfmod))

# p > 0.2 critical value
crit = 1.07 / np.sqrt(len(axpoints))

# Test line for git


## BOOTSTRAP AND KS-TESTS
reps = 10000

boots = model.bootstrap(axpoints, reps)

ecdfboots = np.zeros(boots.shape)
for i in range(ecdfboots.shape[0]):
    ecdfboots[i] = model.generatecdf(np.sort(boots[i]))

## THIS TAKES 37 hours WITH 40000 REPS!!
t = time.time()
cdfp5boot = np.zeros(boots.shape)
for samp in range(cdfp5boot.shape[0]):
    totint = integrate.quad(linrepplusreps5, 0, boots[samp, -1], args=tuple(popt))
    for point in range(cdfp5boot.shape[1]):
        cdfp5boot[samp, point] = integrate.quad(
            linrepplusreps5, 0, boots[samp, point], args=tuple(popt))[0] / totint[0]
print time.time() - t, 's'

Dboots = np.zeros(boots.shape[0])
for i in range(100):
    Dboots[i] = np.max(np.abs(ecdfboots[i] - cdfp5boot[i]))
    
crit02 = 1.07 / np.sqrt(len(axpoints))
len(Dboots[0:100][Dboots[0:100] < crit02]) # = 69

crit005 = 1.36 / np.sqrt(len(axpoints))
len(Dboots[0:100][Dboots[0:100] < crit005]) # = 92

# Generate random samples from PDF to get distribution of K-S D-values
t = time.time()

N = 3918
#mod = linrepnoreps5fixedpeakratios
Dsamp = np.zeros(1000) # Number of D's sampled for.
xmin = 0.
xmax = 100.
pdfint = integrate.quad(mod, xmin, xmax, args=tuple(poptmod))[0]
for D in range(len(Dsamp)):
    xsim = model.rejectionsample(mod, poptmod, pdfint, xmin, xmax, N)[0]
    xsim = np.sort(xsim)
    hsim = np.histogram(xsim, bins=np.arange(fitlength + 1))[0].astype(float)
    cdfsim = model.generatecdf(xsim)
    poptsim, pcovsim = curve_fit(mod, np.arange(fitlength) + 0.5, hsim,
                           p0=(
        20., 3.4,
        1., #1., 1., 1., 1.,# 1., # 1.,
        3., 1.,
        -0.2, 20.
        ),
        bounds=([
        0., 0.,
        0., #0., 0., 0., 0.,# 0., # 0.,
        0., 0.,
        -100., 0.
             ] ,
        [100., 10.,
         1000., #1000., 1000., 1000.,  1000.,# 1000., # 1000.,
         20., 1000.,
         100., 100.
         ])
    )
    #plt.plot(np.arange(fitlength) + 0.5, mod(np.arange(fitlength) + 0.5, *poptsim))
    integral = integrate.quad(mod, 0, xsim[-1], args=tuple(poptsim))[0]
    cdfmod = np.zeros(N)
    cdfmod[0] = integrate.quad(
                mod, 0, xsim[0], args=tuple(poptsim))[0] / integral
    for c in range(1, N):
        cdfmod[c] = cdfmod[c - 1] + integrate.quad(
                mod, xsim[c - 1], xsim[c], args=tuple(poptsim))[0] / integral
    Dsamp[D] = np.max(np.abs(cdfsim - cdfmod))
    if D % 10 == 0:
        print 'Done to sample', D, 'in', time.time() - t, 's.'
print 'This all took', time.time() - t, 's.'
