# -*- coding: utf-8 -*-
"""
Created on Tue Dec 18 11:36:23 2018

@author: fbsacu
"""

import matplotlib.pyplot as plt

## Show all datapoints plus smoothing and models
plt.figure()
axes = plt.subplot(111)
axes.plot(ax, 'k.', markersize=5)
axes.plot(axsmooth, color='xkcd:orangered')
axes.fill_between(x[0:axlength], axsmsort[reps/20, 0:axlength],
                  axsmsort[reps * 19/20, 0:axlength],
                  facecolor='pink')
#axes.plot(linrepplusreps6(x[0:110], *popt6layers),
#          color='xkcd:blue', linestyle='--', linewidth=2)
axes.plot(linrepplusreps5(x[0:90], *popt5layers), color='xkcd:blue',
          #color='chartreuse',
          linestyle='--', linewidth=2)
#axes.plot(linrepplusreps4(x[0:70], *popt4layers), color='fuchsia',
#          linestyle='--', linewidth=2)

### Show RPD, model and components
plt.figure()
axlength = 200
axes = plt.subplot(111)
x = np.arange(axlength)
axes.plot(axsmooth[0:axlength], color='xkcd:red')
reps = axsmsort.shape[0]
axes.fill_between(x[0:axlength], axsmsort[reps/20, 0:axlength],
                  axsmsort[reps * 19/20, 0:axlength],
                  facecolor='xkcd:pink', alpha=0.5)#'pink')
axes.plot(linrepplusreps5(x[0:90], *popt5layers), color='xkcd:blue')

rep, broadening, a, b, c, d, e, locprec, ampreplocs, bgslope, bgoffset = (
        popt5layers)

fitlength = 90
x = np.arange(fitlength)

# Background:
plt.plot(bgoffset + bgslope * x)
# Repeated/mis-localisations
plt.plot(ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2)))
# Repeating peaks:
amps = [a, b, c, d, e]
for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            plt.plot(amp * model.paircorr1d(x, (i + 1) * rep, broadening))    
        else:
            plt.plot(amp * model.gauss1d(x, (i + 1) * rep, broadening))


## Plotting model without repeated localisations:
x = np.arange(fitlength)
axes.plot(linrep5(x[0:fitlength], *poptnoreps), color='chartreuse')
rep, broadening, a, b, c, d, e, bgslope, bgoffset = (
        poptnoreps)

plt.plot(bgoffset + bgslope * x)
amps = [a, b, c, d, e]
for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            plt.plot(amp * model.paircorr1d(x, (i + 1) * rep, broadening))    
        else:
            plt.plot(amp * model.gauss1d(x, (i + 1) * rep, broadening))
            
### Samples of model parameters
fitlength = 90
axes.plot(linrep5offsetnoslope(x[0:90], *popt), 'xkcd:blue')
reps = modsort.shape[0]
axes.fill_between(x[0:fitlength], modsort[reps/20],
                  modsort[reps * 19/20],
                  facecolor='xkcd:lightblue', alpha=0.5)

## After un-detrending:
mod5det = linrep5offsetnoslope(x[0:90], *popt)
lin = model.linfit(x[0:90], *poptlin)
mod5 = mod5det + lin
# Use 5 to 95 % confidence intervals will be at 1.46 SDs from centre.
axes.fill_between(x[0:fitlength], modsort[reps/20] + model.linfit(x[0:90], (poptlin[0] - 1.46 * perrlin[0]), (poptlin[1] - 1.46 * perrlin[1])),
                  modsort[reps * 19/20] + model.linfit(x[0:90], (poptlin[0] + 1.46 * perrlin[0]), (poptlin[1] + 1.46 * perrlin[1])),
                  facecolor='xkcd:lightblue', alpha=0.5)

