# -*- coding: utf-8 -*-
"""Place localisations on a 3D simulated z-disc lattice, find resultant
relative positions and plot distributions. To show how finding relative
positions for certain numbers of localisations or lattice parameters results
in lattice information.

Alistair Curd
University of Leeds
30 July 2018

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""

import numpy as np
import relposdensity3d
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.filters import gaussian_filter1d

labelling = 0.1
detection = 0.1
preciselocs = 0.08 # Estimated from Zhang, mEos2 paper, Nat Meth, Suppl.
fractionused = labelling * detection * preciselocs

# 15 um wide x 2 um deep x 140 nm across x 15 disks

Ndisks = 15 # number of disks
w = 15000 # width of cell
d = 2000 # depth of acquisition
t = 140 # thickness of z-disk

ax = 19.2 # axial lattice constant
trans = 24 # transverse lattice constant
# trans = trans / np.sqrt(2) # Also using anti-parallel set of filaments

# x 6FOVs per experiment
NFOVs = 6

# Iterate over e.g. different cell-transverse lattice constants (e.g. trans)
# or values for pointsperdisk.
# pointsperdisk = 2000
plt.figure()
for trans in np.array([24, 24 / np.sqrt(2)]):
    nw = np.floor(w / trans) # No. lattice points across width of cell
    nd = np.floor(d / trans) # No. lattice points across depth of acquisition
    nt = np.floor(t / ax) # No. lattice points across thickness of z-disk
    totalpoints = nw * nd * nt # No.lattice points in a disk
    
    # No. lattice points simulated per disk.
    # Could be a fraction of points per disk
    # int(totalpoints * fractionused)        
    # pointsperdisk = int(totalpoints * fractionused)
    pointsperdisk = np.int(totalpoints * 0.002) # 1440 for ACTA2 average
    print 'No. points per disk:', pointsperdisk
    
    # Initialise
    axtotsmooth = np.zeros(140)
    normtot = np.zeros((140, 140))
    
    # Iterate over cells    
    for FOV in range(NFOVs):
        print 'FOV', FOV
        ### Initialise neighbour vectors: run first disk
        # Randomise the order of these points for simulation of fractional labelling
        wpoints = np.random.permutation(np.repeat(np.arange(nw), nd * nt)) * trans
        dpoints = np.random.permutation(np.repeat(np.arange(nd), nw * nt)) * trans
        tpoints = np.random.permutation(np.repeat(np.arange(nt), nw * nd)) * ax
        
        # Add effect of localisation precision (3.4, here)
        wpoints = wpoints + np.random.normal(loc=0., scale=3.4, size=len(wpoints))
        dpoints = dpoints + np.random.normal(loc=0., scale=3.4, size=len(dpoints))
        tpoints = tpoints + np.random.normal(loc=0., scale=3.4, size=len(tpoints))
        
        # Simulate a zdisk with pointsperdisk lattice points populated
        sim1disk = np.array(
                     [wpoints[0:pointsperdisk], dpoints[0:pointsperdisk], tpoints[0:pointsperdisk]]).T
        
        # Find the 3D relative positions of the populated points
        nns, filterdist = relposdensity3d.getdistances(sim1disk, 200)
        
        ### Run subsequent disks
        for c in range(1, Ndisks):
            #print c + 1, 'of', Ndisks    
            
            wpoints = np.random.permutation(np.repeat(np.arange(nw), nd * nt)) * trans
            dpoints = np.random.permutation(np.repeat(np.arange(nd), nw * nt)) * trans
            tpoints = np.random.permutation(np.repeat(np.arange(nt), nw * nd)) * ax
            
            # Add effect of localisation precision
            wpoints = wpoints + np.random.normal(loc=0., scale=3.4, size=len(wpoints))
            dpoints = dpoints + np.random.normal(loc=0., scale=3.4, size=len(dpoints))
            tpoints = tpoints + np.random.normal(loc=0., scale=3.4, size=len(tpoints))
            
            # Simulate a zdisk with pointsperdisk lattice points populated
            sim1disk = np.array(
                         [wpoints[0:pointsperdisk], dpoints[0:pointsperdisk], tpoints[0:pointsperdisk]]).T
            
            # Find the 3D relative positions of the populated points
            nns = np.append(nns, relposdensity3d.getdistances(sim1disk, 200)[0], axis=0)
        
        # Run analysis as in zdisc.py
        v, z, h = nns.T
        
        nns = pd.DataFrame({'axial': h, 'tocoverslip': z, 'perpaxis2D': v})
        nns['transverse'] = np.sqrt(nns.perpaxis2D**2 + nns.tocoverslip**2)
        nns.axial = abs(nns.axial)
        
        hist, edges = np.histogramdd(np.column_stack((nns.transverse, nns.axial)),
                          bins=(np.arange(0, 251, 1), np.arange(0, 251, 1)))
    
        axdensity = np.sum(hist[0:10, 0:140], axis=0)
        axfit = np.polyfit(np.arange(140), axdensity[0:140], 1) # fit over likely z-disc width
        axfitted = axdensity - (np.arange(140) * axfit[0] + axfit[1])
        axsmooth = gaussian_filter1d(axfitted, 4.8)
        
        axtotsmooth = axtotsmooth + axsmooth
    
        normhist = np.zeros(hist.shape)
        for num, row in enumerate(hist):
            normhist[num] = row / (2. * num + 1.)
        
        normhist = normhist[0:140, 0:140]
        
        normtot = normtot + normhist

    # Cell-axial 1D plot    
    #plt.figure()
    #plt.plot(axtotsmooth, 'k')
    
    # Axial repeat plot
    #seqax = np.arange(1, 7)
    #mod192, edge = np.histogram(seqax * 19.2, np.arange(140))
    #plt.plot(-1.2 + gaussian_filter1d(mod192 * 8., 4.8), 'r--')
    
    
    # Cell-transverse 1D plot
    #plt.figure()
    #plt.plot(gaussian_filter1d(np.sum(normtot[:, 0:10], axis=1), 4.8))
    
    # 2D (cell-axial, cell-transverse) plot
    #plt.figure()
    #plt.matshow(gaussian_filter(normtot, 4.8), cmap='inferno', vmin=0)  
