# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 16:21:41 2018

@author: fbsacu
"""

import numpy as np

diameter = 300.
rad = diameter / 2.

x = np.random.uniform(-rad, rad, 1000)
y = np.random.uniform(-rad, rad, 1000)

keep = x ** 2 + y ** 2 < rad ** 2

coords = np.column_stack((x[keep], y[keep], np.zeros(len(x[keep]))))

d, filterdist = getdistances(coords, 400)
simhist = np.histogram(np.sqrt(d[:, 0] ** 2 + d[:, 1] ** 2),
                       bins=np.arange(400.))[0].astype(float)
scalesim = simhist / 2000.


def RPDcirc(d, R):
    """See journal and lab book.
    Results in amplitude of 1e7
    """
    rpd = 2 * np.pi * d * (
        (np.arccos(d / (2 * R)) -
        np.arctan(d / np.sqrt(4 * R ** 2 - d ** 2)) +
        np.pi / 2
        ) * R ** 2 -
        d * np.sqrt(4 * R ** 2 - d ** 2) / 2
        ) * 10 ** -7
    return rpd

def internalbg(sep, dia, A):
    bg = A * RPDcirc(d=np.arange(np.round(dia)), R=dia/2.)
    rpd = np.pad(bg, (0, len(sep) - len(bg)), 'constant')
    return rpd

sep = np.arange(399.)
popt, pcov = curve_fit(internalbg, sep, scalesim,
                       p0=(100., 1.), bounds=(0, [1000., 10.]))
plt.plot(internalbg(sep, *popt))
perr = np.sqrt(np.diag(pcov))
