# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 16:43:22 2018

@author: fbsacu
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d
import modelling_general as model
from scipy.optimize import curve_fit
from scipy.stats import ks_2samp
from scipy.stats import shapiro



axlength = 201 ## TODO User input for this
x = np.arange(axlength)
relpos = pd.read_pickle('S:/Peckham/Bioimaging/Alistair/PALM-STORM/AlexasNearestNeighbours/LASP2/nns-aligned-5fovs.pkl')

relpos.axial = abs(relpos.axial)
        
# 2D density (1-nm bin histogram) of cell-axial/cell-tranverse relative
# positions
hist, edges = np.histogramdd(np.column_stack(
                  (relpos.transverse, relpos.axial)),
                  bins=(np.arange(0, 251, 1), np.arange(0, 251, 1)))

# Cell-axial profile
axlasp = np.sum(hist[0:10, 0:(axlength + 1)], axis=0)
axlasp = axlasp / 2. # Remove duplicates
axlaspsmooth = gaussian_filter1d(axlasp, 4.8)

def linrepplusreps5(x, rep, broadening,
                   a, b, c, d, e,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps5offset(x, rep, broadening,
                   offset,
                   a, b, c, d, e,# f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]#, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps

    return rpd

def linrep5offset(x, rep, broadening,
                   offset,
                   a, b, c, d, e, #f,
                   #locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]#, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background

    return rpd

def linrep5offsetnoslope(x, rep, broadening,
                   offset,
                   a, b, c, d, e, #f,
                   #locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]#, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    rpd = rpd + bgoffset

    return rpd

def linrep5offsetplusrepsnoslope(x, rep, broadening,
                   offset,
                   a, b, c, d, e, #f,
                   locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]#, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    rpd = rpd + bgoffset
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps

    return rpd

def linrep5(x, rep, broadening,
                   a, b, c, d, e,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps6offset(x, rep, broadening,
                          offset,
                   a, b, c, d, e, f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

fitlength = 90
popt, pcov = curve_fit(
    linrep5offsetnoslope, x[0:fitlength], axsmooth[0:fitlength],
    p0=(20., 3.4,
        3.,
        1., 1., 1., 1., 1.,# 1.,# 1.,
        #3., 1.,
        -2.#, 20.
        ),
    bounds=([0., 0.,
             0.,
             0., 0., 0., 0., 0.,# 0.,# 0.,
             #0., 0.,
             -10.,# 0.
             ] ,
        [100., 10.,
         15.,
         1000., 1000., 1000., 1000.,  1000.,# 1000.,# 1000.,
         #20., 1000.,
         0.,# 100.
         ])
    )
    
#plt.plot(linrepplusreps5offset(x[0:90], *popt))
#plt.plot(linrepplusreps6offset(x[0:110], *popt))
perr = np.sqrt(np.diag(pcov))
params = np.column_stack((popt, perr))
ssr = np.sum((linrepplusreps(x, *popt) - axlasp) ** 2)

### Linear fit
poptlin, pcovlin = curve_fit(model.linfit, x[0:fitlength],
                             axlaspsmooth[0:fitlength])
perrlin = np.sqrt(np.diag(pcovlin))
paramslin = np.column_stack((poptlin, perrlin))

### Detrend
rpddetlasp = axlaspsmooth[0:fitlength] - model.linfit(x[0:fitlength], *poptlin)
#rpddet = rpddet - np.min(rpddet) # Make above zero

### Bootstrap sampling of relative positions

reps = 100000
bootslasp = model.bootstrap(relpos.axial[relpos.transverse < 10.], reps)


# Calculate bootstrap smoothed profiles
axsmoothblasp = np.zeros((reps, axlength))
axblasp = np.zeros((reps, axlength))
for i in range(reps):
    hist, edges = np.histogram(bootslasp[i], bins=(np.arange(0, axlength + 1, 1)))
    axblasp[i] = hist / 2. # Remove duplicates
    axsmoothblasp[i] = gaussian_filter1d(axblasp[i], 4.8)
axsmsortlasp = np.sort(axsmoothblasp, axis=0)

# Sample model parameters
reps = 100000
poptsampleslasp = np.zeros((reps, len(popt)))
modsampleslasp = np.zeros((reps, fitlength))

for i in range(reps):
    for p in range(len(popt)):
        poptsampleslasp[i, p] = np.random.normal(loc=popt[p], scale=perr[p])
    modsampleslasp[i] = linrep5offsetnoslope(x[0:fitlength], *poptsampleslasp[i])

modsortlasp = np.sort(modsampleslasp, axis=0)

# Detrend RPDs from bootstrapping:
rpddetbsortlasp = np.zeros((reps, fitlength))

for i in range(reps):
    rpddetbsortlasp[i] = axsmsortlasp[i, 0:90] - model.linfit(x[0:fitlength], *poptlin)

statslasp = np.zeros((reps, 2))
for i in range(reps):
    statslasp[i] = ks_2samp(rpddetbsortlasp[i, 0:90],
             linrep5offsetnoslope(x[0:fitlength], *popt))
    
float(len(statslasp[statslasp[:, 1] > 0.05])) / reps


# Un-detrend!
mod5det = linrep5offsetnoslope(x[0:90], *popt)
lin = model.linfit(x[0:90], *poptlin)
mod5 = mod5det + lin



(rep, broadening, offset,
 a, b, c, d, e, locprec, ampreplocs, bgslope, bgoffset) = popt
axes.plot(bgoffset + bgslope * x)
