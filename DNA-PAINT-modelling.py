# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 16:32:11 2019

@author: fbsacu
"""

import numpy as np
import matplotlib.pyplot as plt
import h5py
from relposdensity3d import getdistances
import modelling_general as model
from scipy.optimize import curve_fit

"""
### GET DATA
f = h5py.File('S:/Peckham/Bioimaging2/Alistair/DNA-origami-RalfJungmann/Unknown_2019-06-17/dataset2_DNA-PAINT.hdf5')
locs = f['locs']
locs2 = np.zeros((len(locs), 3))
for i in range(len(locs)):
    locs2[i] = [locs[i][1], locs[i][2], locs[i][11]]
locs2 = locs2[::5] # Too many locs. Finding relpos took too long.
xyzscaled = locs2 * [130., 130., 1.] # 130 x 130 x 1 nm per unit dimension
np.savetxt('S:/Bioimaging/2015/Alistair-Active2019/dataset2_DNA-PAINT_xyz_1in5_2844locs.csv', xyzscaled, delimiter=',', header='x, y, z')
run relposdensity3d # use filterdist=400


"""

def triprismvertices(a, b):
    """Model triangular prismm with triangular sides a and connecting sides b.
    End on."""
    vv = np.zeros((6, 3))
    # vv[0] is [0, 0, 0]
    vv[1] = [a, 0, 0]
    vv[2] = [a / 2, a * np.sqrt(3) / 2, 0]
    vv[3] = [0, 0, b]
    vv[4] = vv[1] + vv[3]
    vv[5] = vv[2] + vv[3]
    return vv

def cuboidvertices(a, b, c):
    """Model cuboid with sides a, b and c."""
    vv = np.zeros((8, 3))
    # vv[0] is [0, 0, 0]
    vv[1] = [a, 0, 0]
    vv[2] = [0, b, 0]
    vv[3] = [a, b, 0]
    vv[4] = [0, 0, c]
    vv[5] = vv[1] + vv[4]
    vv[6] = vv[2] + vv[4]
    vv[7] = vv[3] + vv[4]
    return vv

def tripyramidvertices(a, b):
    """Model triangular pyramid with base sides a and height b."""
    vv = np.zeros((4, 3))
    # vv[0] is [0,0, 0]
    vv[1] = [a, 0, 0]
    vv[2] = [a / 2, a * np.sqrt(3) / 2, 0]
    vv[3] = [a / 2, a * np.sqrt(3) / 4, b]
    return vv

def get1drelposnofilt(xyz):
    """Store all relative positions in a numpy array. No need for a filter
    distance for searching.

    Args:
        xyz: numpy array of localisations with shape (N, 3),
        where N is the number of vertices.
    
    Returns:
        Numpy (N) array of Euclidean distances between vertices.  
    """
    relpos = xyz - xyz[0]  
    for i, loc in enumerate(xyz[1:len(xyz)]):
        relpos = np.append(relpos, xyz - loc, axis=0)
    
    # Remove [0., 0.] relative positions (self-referencing)    
    relpos = relpos[np.any(relpos != 0., axis=1)]
    
    return(relpos)

def triprismrpd(r, a, b, locamp, locprec, structamp, spread):
    """r are distances over which the model needs to be evaluated,
    e.g. 0.5, 1.5, 2.5, 3.5, ... nm
    """
    # Set up distances between tringular prism vertices
    verts = triprismvertices(a, b)
    relpos = get1drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2 + relpos[:, 2] ** 2)
    # Initialise rpd array
    rpd = r * 0.
    # Fill with pair correlations between 3D Gaussian spreads at vertices
    for i, d in enumerate(dists):
        rpd = rpd + structamp * model.paircorr3d(r, d, spread)
    # Include single molecule localisation precision
    # This is approximated to isotropic
    rpd = rpd + locamp * model.paircorr3d(r, 0., np.sqrt(2) * locprec)
    
    return rpd

def triprismongridrpd(r, a, b, locamp, locprec, structamp, spread,
                   gridspace, gridamp, gridspread):
    """r are distances over which the model needs to be evaluated,
    e.g. 0.5, 1.5, 2.5, 3.5, ... nm
    a: triangular side length
    b: connecting side length
    locamp: Amplitude of sinlge molecule localisation precision component
    locprec: Average single molecule localisation precision
    structamp: Amplitude of components reflecting the structural features
        of the complex.
    spread: Spread owing to unresolvable complexity or inhomogeneity between
        complexes.
    gridspace: The spacing of a square grid the complexes are found on.
    gridamp: Amplitude of components reflecting the nieghbouring complexes at
        nearby grid points.
    gridspread: Spread owing to different orientation at different grid points.
    
    """
    # Set up distances between tringular prism vertices
    verts = triprismvertices(a, b)
    relpos = get1drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2 + relpos[:, 2] ** 2)
    # Initialise rpd array
    rpd = r * 0.
    # Fill with pair correlations between 3D Gaussian spreads at vertices
    for i, d in enumerate(dists):
        rpd = rpd + structamp * model.paircorr3d(r, d, spread)
    # Include single molecule localisation precision
    # This is approximated to isotropic
    rpd = rpd + locamp * model.paircorr3d(r, 0., np.sqrt(2) * locprec)
    
    # Include neighbouring complexes on square grid:
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace, gridspread)
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace * np.sqrt(2),
                                           gridspread)
    
    return rpd
   
def triprismongridonelengthrpd(r, a, locamp, locprec, structamp, spread,
                   gridspace, gridamp, gridspread):
    """r are distances over which the model needs to be evaluated,
    e.g. 0.5, 1.5, 2.5, 3.5, ... nm
    a: triangular side length
    b: connecting side length
    locamp: Amplitude of sinlge molecule localisation precision component
    locprec: Average single molecule localisation precision
    structamp: Amplitude of components reflecting the structural features
        of the complex.
    spread: Spread owing to unresolvable complexity or inhomogeneity between
        complexes.
    gridspace: The spacing of a square grid the complexes are found on.
    gridamp: Amplitude of components reflecting the nieghbouring complexes at
        nearby grid points.
    gridspread: Spread owing to different orientation at different grid points.
    
    """
    # Set up distances between tringular prism vertices
    verts = triprismvertices(a, a)
    relpos = get1drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2 + relpos[:, 2] ** 2)
    # Initialise rpd array
    rpd = r * 0.
    # Fill with pair correlations between 3D Gaussian spreads at vertices
    for i, d in enumerate(dists):
        rpd = rpd + structamp * model.paircorr3d(r, d, spread)
    # Include single molecule localisation precision
    # This is approximated to isotropic
    rpd = rpd + locamp * model.paircorr3d(r, 0., np.sqrt(2) * locprec)
    
    # Include neighbouring complexes on square grid:
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace, gridspread)
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace * np.sqrt(2),
                                           gridspread)
    
    return rpd

def tripyramidongridrpd(r, a, b, locamp, locprec, structamp, spread,
                   gridspace, gridamp, gridspread):
    """r are distances over which the model needs to be evaluated,
    e.g. 0.5, 1.5, 2.5, 3.5, ... nm
    a: triangular side length
    b: connecting side length
    locamp: Amplitude of sinlge molecule localisation precision component
    locprec: Average single molecule localisation precision
    structamp: Amplitude of components reflecting the structural features
        of the complex.
    spread: Spread owing to unresolvable complexity or inhomogeneity between
        complexes.
    gridspace: The spacing of a square grid the complexes are found on.
    gridamp: Amplitude of components reflecting the nieghbouring complexes at
        nearby grid points.
    gridspread: Spread owing to different orientation at different grid points.
    
    """
    # Set up distances between tringular prism vertices
    verts = tripyramidvertices(a, b)
    relpos = get1drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2 + relpos[:, 2] ** 2)
    # Initialise rpd array
    rpd = r * 0.
    # Fill with pair correlations between 3D Gaussian spreads at vertices
    for i, d in enumerate(dists):
        rpd = rpd + structamp * model.paircorr3d(r, d, spread)
    # Include single molecule localisation precision
    # This is approximated to isotropic
    rpd = rpd + locamp * model.paircorr3d(r, 0., np.sqrt(2) * locprec)
    
    # Include neighbouring complexes on square grid:
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace, gridspread)
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace * np.sqrt(2),
                                           gridspread)
    
    return rpd

def cuboidongridrpd(r, a, b, c, locamp, locprec, structamp, spread,
                   gridspace, gridamp, gridspread):
    """r are distances over which the model needs to be evaluated,
    e.g. 0.5, 1.5, 2.5, 3.5, ... nm
    a, b, c: cuboid side lengths
    locamp: Amplitude of sinlge molecule localisation precision component
    locprec: Average single molecule localisation precision
    structamp: Amplitude of components reflecting the structural features
        of the complex.
    spread: Spread owing to unresolvable complexity or inhomogeneity between
        complexes.
    gridspace: The spacing of a square grid the complexes are found on.
    gridamp: Amplitude of components reflecting the nieghbouring complexes at
        nearby grid points.
    gridspread: Spread owing to different orientation at different grid points.
    
    """
    # Set up distances between tringular prism vertices
    verts = cuboidvertices(a, b, c)
    relpos = get1drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2 + relpos[:, 2] ** 2)
    # Initialise rpd array
    rpd = r * 0.
    # Fill with pair correlations between 3D Gaussian spreads at vertices
    for i, d in enumerate(dists):
        rpd = rpd + structamp * model.paircorr3d(r, d, spread)
    # Include single molecule localisation precision
    # This is approximated to isotropic
    rpd = rpd + locamp * model.paircorr3d(r, 0., np.sqrt(2) * locprec)
    
    # Include neighbouring complexes on square grid:
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace, gridspread)
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace * np.sqrt(2),
                                           gridspread)
    
    return rpd

def cuboidongridsquarebaserpd(r, a, b, locamp, locprec, structamp, spread,
                   gridspace, gridamp, gridspread):
    """r are distances over which the model needs to be evaluated,
    e.g. 0.5, 1.5, 2.5, 3.5, ... nm
    a, b, c: cuboid side lengths
    locamp: Amplitude of sinlge molecule localisation precision component
    locprec: Average single molecule localisation precision
    structamp: Amplitude of components reflecting the structural features
        of the complex.
    spread: Spread owing to unresolvable complexity or inhomogeneity between
        complexes.
    gridspace: The spacing of a square grid the complexes are found on.
    gridamp: Amplitude of components reflecting the nieghbouring complexes at
        nearby grid points.
    gridspread: Spread owing to different orientation at different grid points.
    
    """
    # Set up distances between tringular prism vertices
    verts = cuboidvertices(a, a, b)
    relpos = get1drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2 + relpos[:, 2] ** 2)
    # Initialise rpd array
    rpd = r * 0.
    # Fill with pair correlations between 3D Gaussian spreads at vertices
    for i, d in enumerate(dists):
        rpd = rpd + structamp * model.paircorr3d(r, d, spread)
    # Include single molecule localisation precision
    # This is approximated to isotropic
    rpd = rpd + locamp * model.paircorr3d(r, 0., np.sqrt(2) * locprec)
    
    # Include neighbouring complexes on square grid:
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace, gridspread)
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace * np.sqrt(2),
                                           gridspread)
    
    return rpd

def cubeongridrpd(r, a, locamp, locprec, structamp, spread,
                   gridspace, gridamp, gridspread):
    """r are distances over which the model needs to be evaluated,
    e.g. 0.5, 1.5, 2.5, 3.5, ... nm
    a, b, c: cuboid side lengths
    locamp: Amplitude of sinlge molecule localisation precision component
    locprec: Average single molecule localisation precision
    structamp: Amplitude of components reflecting the structural features
        of the complex.
    spread: Spread owing to unresolvable complexity or inhomogeneity between
        complexes.
    gridspace: The spacing of a square grid the complexes are found on.
    gridamp: Amplitude of components reflecting the nieghbouring complexes at
        nearby grid points.
    gridspread: Spread owing to different orientation at different grid points.
    
    """
    # Set up distances between tringular prism vertices
    verts = cuboidvertices(a, a, a)
    relpos = get1drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2 + relpos[:, 2] ** 2)
    # Initialise rpd array
    rpd = r * 0.
    # Fill with pair correlations between 3D Gaussian spreads at vertices
    for i, d in enumerate(dists):
        rpd = rpd + structamp * model.paircorr3d(r, d, spread)
    # Include single molecule localisation precision
    # This is approximated to isotropic
    rpd = rpd + locamp * model.paircorr3d(r, 0., np.sqrt(2) * locprec)
    
    # Include neighbouring complexes on square grid:
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace, gridspread)
    rpd = rpd + gridamp * model.paircorr2d(r, gridspace * np.sqrt(2),
                                           gridspread)
    
    return rpd

""" GET RELPOSs
relpos = np.loadtxt('S:/Bioimaging/2015/Alistair-Active2019/dataset1_DNA-PAINT_xyz_1in10_2232locs_PERPL-relposns_400.0filter.csv', delimiter=',')
# This file does not contain duplicates in [dx, dy, dz]
d = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2 + relpos[:, 2] ** 2)
fitlength = 400.
h = plt.hist(d, bins=np.arange(fitlength + 1),
             weights=np.repeat(float(fitlength) / len(d),
                               len(d)),
             color='lightblue')[0]
"""

### FIT

mod = triprismongridonelengthrpd

def fitmodel_to_hist(distancehist, mod=triprismongridrpd, fitlength=400.):
    poptmod, pcovmod = curve_fit(
        mod, np.arange(fitlength) + 0.5, distancehist,
        p0=(
        100., #100., #100., # a, b, c
        500., 10., # locamp, locprec
        500., 20., # structamp, spread
        250., 500., 250., # gridspace, gridamp, gridspread
            ),
        bounds=(
            [
            0., #0., #0., # a, b, c
            0., 0., # locamp, locprec
            0., 0., # structamp, spread
            0., 0., 0., # gridspace, gridamp, gridspread
            ],
            [
            400., #200., #200., # a, b, c
            1000., 50., # locamp, locprec
            1000., 100., # structamp, spread
            500., 1000., 500., # gridspace, gridamp, gridspread
             ])
        )
    plt.plot(np.arange(fitlength) + 0.5, mod(np.arange(fitlength) + 0.5, *poptmod))
    perrmod = np.sqrt(np.diag(pcovmod))
    params = np.column_stack((poptmod, perrmod))
    print params
    #totint = [1.]
    #intquad = integrate.quad(mod, 0, fitlength, args=tuple(poptmod))
    #intstep = np.sum(mod(np.arange(0., fitlength, 0.001), *poptmod)) / 1000.
    k = float(len(poptmod) + 1) # No. free parameters, including var. of residuals
                         # for least squares fit.
    #aic = -2 * np.sum(np.log(mod(axpoints, *poptmod) / intstep)) + (
    #            2 * k)
    ssr = np.sum((mod(np.arange(fitlength) + 0.5, *poptmod) -
                  distancehist) ** 2)
    aic = fitlength * np.log(ssr / fitlength) + 2 * k
    aiccorr = aic + 2 * k * (k + 1) / (fitlength - k - 1)
    #aiccorr = aic + 2 * k * (k + 1) / (len(axpoints) - k - 1)
    #print 'intquad =', intquad[0]
    #print 'intstep =', intstep
    print 'SSR = ', ssr
    print 'AIC =', aic
    print 'AICcorr =', aiccorr
    
    return ()
