# -*- coding: utf-8 -*-
"""zdisc.py

Functions to aggregate relative positions of protein
localisations from striated muscle FOVs, and display and analyse distributions.
Also relevant for FOVs in general which need to be aligned for aggregation of data.

Requires relposdensity3d.py, included alongside this script.

Alistair Curd
University of Leeds
30 July 2018

---
Copyright 2018 Peckham Lab

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.filters import gaussian_filter1d
from scipy.ndimage.filters import maximum_filter
import cPickle
from relposdensity3d import getdistances
   
## Rotation of FOV to choose when cell is pointing horizontally:
#from scipy.ndimage import rotate
#plt.matshow(rotate(histflat, angle, order=0), cmap='inferno')   
   
def rotatecoords(x, y, z, angle=0):
    """ Rotate coordinates about z axis.
    Input angle of rotation is in degrees,
    same as FOV rotation to get cell sideways.
    
    
    
    Args:
        x, y, z coordinates:
            x is horizontal on FOV, y is vertical.
        angle:
            Angle of rotation to apply.
            Angles are defined from horizontal, matching the convention of
            scipy.ndimage.rotate(3Dhistogram),
            since matplotlib.pyplot.matshow plots y pointing downwards,
            this has the appearance of defining negative as clockwise
            (but if y pointed upwards, positive angles would be clockwise
            from horizontal)
                
    Returns:
        x, y, z coordinates after rotation.
    """
    if angle == 0:
        print 'Please tell me the angle to rotate'
        angle = float(raw_input(
'(anti-clockwise about depth axis, when vertical axis points downwards): '))

    r = np.sqrt(x ** 2 + y ** 2)
    # Minus sign to match convention in FOV images rotated earlier
    theta = -np.arctan(y / x) # This may result in an error for zeros in x
    # but it sets the right length, and zeros are addressed below.
    theta[(y == 0)] = 0
    theta[(y == 0) & (x < 0)] = np.pi
    theta[(x == 0) & (y > 0)] = -np.pi / 2
    theta[(x == 0) & (y < 0)] = np.pi / 2
    theta[(x < 0)] = theta[(x < 0)] + np.pi
    angle = angle * np.pi / 180
    newtheta = theta + angle
    xout = r * np.cos(newtheta)
    yout = r * np.sin(newtheta) * -1 # * -1 to avoid flipping with
                                     # this angle convention
    zout = z
    
    return(xout, yout, zout)

def axialtransversehist(nns, angle):
    """Finds density of relative positions of localisations across a cell
    (perpendicular to the axis), i.e. integrating around all angles on the
    z-disc plane. Divides integrated count by distance across the z-disc plane
    to account for the circle being counted over (area of integrated anulus
    = 2.pi.R.dR + pi.dR^2; when dR = 1, as here, area = 2.pi.(R+0.5))
    
    Args:
        nns: Relative positions of nearby localisations
        angle: How far the relative position vectors need to be rotated about z
            (distance from coverslip) to make the cell point sideways.
    
    Returns:
        normhist: 2D histogram contains densities of relative positions in
            axial-transverse space. First dimension is 'radial' (transverse).
        normblur: normhist with gaussian blur, sigma=3

    Prints but does not return:
        A smoothened version of normhist      
    """
    y, x, z = np.transpose(nns)
    
    # Refractive index of z-disc and compensate for difference with water
    nz = 1.37 # Sidick et al., Biophys. J. 66 (1994), 2051-61
    z = z * nz / 1.33
    
    x, y, z = rotatecoords(x, y, z, angle)
    
    axial = abs(x)
    radial = np.sqrt(y ** 2 + z ** 2)
    
    # Radial as first dimension so gets plotted vertically
    # bins include left edge, but not right edge, except for last bin
    hist, edges = np.histogramdd(np.column_stack((radial, axial)),
                      bins=(np.arange(0, 250, 1), np.arange(0, 250, 1)))
    # For normalise radial dimension of histogram (divide by 2 * pi * R)
    # Use the middle of the bin as R
    # This is the same as the area of the search ring
    # See docstring for this function
    normhist = np.zeros(hist.shape)
    for num, row in enumerate(hist):
        normhist[num] = row / (num + 0.5)
    
    # Make (0, 0) a maximum because it's less confusing
    normhist[0, 0] = np.max(normhist)
    normblur = gaussian_filter(normhist, sigma=6, mode='nearest')
    
    keepers = np.ones(normblur.shape, dtype='bool')
    keepers[0:10, 0:10] = 0
    setmax = np.max(normblur[keepers])
    
    plt.matshow(normblur[0:200, 0:200], cmap='inferno',
                vmax=setmax)
    plt.tick_params(labelleft='off', labeltop='off')
    
    return(normhist, normblur)

def relposaxtrans(listoffiles):
    """Calculates 3D relative positions and cell-axial and cell-transverse
    relative positions for pre-filtered localisations from Z-disks in a
    cardiomyocyte FOV. Saves the relative positions for each FOV separately as
    a pandas Dataframe.
    
    Arguments:
        listoffiles: List of paths to files where the pre-filtered
            localisations are to be found (output from palm3dextra.py)
    
    Returns:
        nns: The relative positions (pandas DataFrame calculated from the
            last input file in the list.
    """
    
    # Refractive index of z-disc
    nz = 1.37 # Sidick et al., Biophys. J. 66 (1994), 2051-61
    
    # Use FOVs cropped or filtered (hence subset) to remove fiducial markers
    # and low-precision locs. Uses output from palm3dextra.py.
    for subsetpklfile in listoffiles:
        print subsetpklfile
        with open(subsetpklfile, 'rb') as f:
            subset = pd.read_pickle(f)
        
        # Find 3d relative positions. Don't forget 'y' was horizontal on the
        # screen in palm3d.
        vhz = np.column_stack((subset['xnm'], subset['ynm'], subset['znm']))
        nns, filterdist = getdistances(vhz)
        
        # Rotate relative positions so that distance along the cell-axis is now
        # distance along a coordinate 'h'
        v, h, z = np.transpose(nns)
        angle = float(raw_input('What is the angle of rotation? '))
        h, v, z = rotatecoords(h, v, z, angle)
        nns = pd.DataFrame({'axial': h, 'tocoverslip': z, 'perpaxis2D': v})
        
        # Adjust depth for refractive index of Z-disk, different from water
        nns['z_nzcorr'] = nns.tocoverslip * nz/1.33
        
        # Calculate cell-transverse distances
        nns['transverse'] = np.sqrt(nns.perpaxis2D**2 + nns.z_nzcorr**2)
        
        # Save 3d and cell-transverse relative positions
        nnsfile = subsetpklfile.replace('.pkl', '_NNS_Rot%i_len%i.pkl' % (
                                                  int(round(angle)), len(nns)))
        nns.to_pickle(nnsfile)
    
    return nns

def aggregaterelposdensities(nnsfiles):
    """Combine relative position results from multiple cardiomyocytes, ready
    for assessment of cell-axial, cell-transverse and 2D distributions. Plots
    of these profiles for individual cells can also be produced by uncommenting
    some lines. A 4.8 nm blur was used when plotting to match sqrt(2) * mean
    precision estimate of filtered ACTA2 localisations.
    
    Arguments:
        nnsfiles:
            List of paths to files containing the cell-axial and
            cell-transverse relative positions from Z-disk localisations, in a
            pandas Dataframe.
    
    Returns:
        axtot: Detrended cell-axial relative position density profile
        normtot: Normalised (in cell-transverse direction) 2D relative
            position density profile.        
    """    
    # Iterate through FOVs.
    # Supply nnsfiles: list of files containing output of required near
    # neighbour relative positions.
    for c, nnsfile in enumerate(nnsfiles):
        if c == 0:
            nns = pd.read_pickle(nnsfile)
            savefile = nnsfile.replace('.pkl',
                                       'Aggregated_%i_cells.pkl'
                                       % len(nnsfiles))
        else:
            nns = nns.append(pd.read_pickle(nnsfile))
        # Testing:
        #print ('FOV:', c, ', shape:', pd.read_pickle(nnsfile).shape,
        #       'total size:', nns.shape)

    print 'Aggregated data from', c + 1, 'FOVs.'
    
    nns.to_pickle(savefile)
    
    return nns

def aggregaterelposdensities_normanddetrend(nnsfiles):
    """Combine relative position results from multiple cardiomyocytes, ready
    for assessment of cell-axial, cell-transverse and 2D distributions. Plots
    of these profiles for individual cells can also be produced by uncommenting
    some lines. A 4.8 nm blur was used when plotting to match sqrt(2) * mean
    precision estimate of filtered ACTA2 localisations.
    
    Arguments:
        nnsfiles:
            List of paths to files containing the cell-axial and
            cell-transverse relative positions from Z-disk localisations, in a
            pandas Dataframe.
    
    Returns:
        axtot: Detrended cell-axial relative position density profile
        normtot: Normalised (in cell-transverse direction) 2D relative
            position density profile.        
    """    
    
    # Initialise.
    axlength = int(raw_input('Maximum axial separation to include (int)? '))
    translength = int(raw_input(
            'Maximum transverse separation to include (int)? '))
    axtot = np.zeros(axlength + 1)
    normtot = np.zeros((translength + 1, axlength + 1))
    
    # Iterate through FOVs.
    # Supply nnsfiles: list of files containing output of required near
    # neighbour relative positions.
    for c, nnsfile in enumerate(nnsfiles):
        with open(nnsfile, 'rb') as f:
            # nns = cPickle.load(f) # Works for saving and loadingwith the
                                    # same version of pickle and pandas
            nns = pd.read_pickle(f) # For compatibility between versions
        
        print 'Cell %i of %i.' % (c + 1, len(nnsfiles))
        # Axial had negative values, symmetric with positive values, with
        # identical associated cell-tranverse distances.
        nns.axial = abs(nns.axial)
        
        # 2D density (1-nm bin histogram) of cell-axial/cell-tranverse relative
        # positions
        hist, edges = np.histogramdd(np.column_stack(
                          (nns.transverse, nns.axial)),
                          bins=(np.arange(0, 251, 1), np.arange(0, 251, 1)))
        
        # Cell-axial profile
        ax = np.sum(hist[0:10, 0:(axlength + 1)], axis=0)
        
        # Detrend, since counts will decrease linearly with increasing
        # cell-axial separation, because of finite thickness of Z-disk.
        # 140 is likely z-disc width
        axfit = np.polyfit(np.arange(axlength + 1), ax[0:(axlength + 1)], 1) 
        axfitted = ax - (np.arange(axlength + 1) * axfit[0] + axfit[1])
        
        ## Uncomment as appropriate to get multiple smoothened axial profile
        ## plots.    
        #axsmooth = gaussian_filter1d(axfitted, 4.8)
        #plt.figure()
        #plt.plot(axsmooth)
        axtot = axtot + axfitted    
    
        # Normalised in cell-transverse direction for the 2D radial
        # distribution function for a 2D homogeneous, isotropic localisation
        # density
        normhist = np.zeros(hist.shape)
        for num, row in enumerate(hist):
            normhist[num] = row / (2. * num + 1.)
        
        # Crop to 200 / sqrt(2), to avoid edge artefacts resulting from
        # rotation of # coordinates.
        normhist = normhist[0:(translength + 1), 0:(axlength + 1)]
    
        ## Uncomment as appropriate to get multiple smoothened transverse
        ## profile plots.    
        #plt.figure()
        #plt.plot(gaussian_filter1d(np.sum(normhist[:, 0:10], axis=1), 4.8))
        
        ## Uncomment as appropriate to get multiple 2D density plots. 
        #plt.figure()
        #plt.matshow(gaussian_filter(normhist, 4.8), cmap='inferno')    
        
        normtot = normtot + normhist
    
    return (axtot, normtot)


#################################################
####### FOR FITTING PEAKS TO A REPEAT ###########
##### ALSO FIND THE RESIDUALS OF SUCH A FIT #####
########## TO ASSESS IT##########################
#################################################

### Without offset (used for ACTA2)
def fitpeaksnooffset(peaks):
    """Fit the positions of local maxima to a repeating distance, starting from
    zero, as used for ACTA2. Residuals are standardised with
    sqrt(summed squares / N), as in SD.
    
    Args:
        peaks: List of postions of local maxima
    
    Returns:
        m: Best fit repeating distance
        sdr: Standard deviation of the residuals of the fit.
    """
    
    # Fit straight line.
    # y dependent on x, no constant term (multiple of zero) in linear fit.
    x = np.arange(1, len(peaks) + 1)
    y = peaks
    A = np.vstack([x, np.zeros(len(x))]).T
    m, c = np.linalg.lstsq(A, y)[0]
    sdr = np.sqrt(np.sum((y - m * x) ** 2) / len(y))
    return m, sdr
    
### With offset (used for LASP2):
def fitpeakswithoffset(peaks):
    """Fit the positions of local maxima to a repeating distance, allowing an
    offset from zero as the starting position for the repeat, as used for
    LASP2 data. Residuals are standardised with sqrt(summed squares / N), as
    in SD.
    
    Args:
        peaks: List of postions of local maxima
    
    Returns:
        m: Best fit repeating distance
        c: Non-zero (generally) offset to start of repeat
        sdr: Standard deviation of the residuals of the fit.
    """

    # Fit straight line.
    # y dependent on x, constant term (multiple of one) in linear fit.
    x = np.arange(len(peaks))
    y = peaks
    A = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(A, y)[0]
    sdr = np.sqrt(np.sum((y - (m * x + c)) ** 2) / len(y))
    return m, c, sdr      

###############################################
###  GENERATE RPD PLOTS FOR AGGREGATED FOVs ###
###############################################
"""These lines generate RPD plots for aggregated FOVs:

# Aggregated cell-axial profile
plt.figure()
axtotsmooth = gaussian_filter1d(axtot, 4.8)
plt.plot(axtotsmooth, 'k-')

# Aggregated cell-transverse profile
plt.figure()
transsmooth = gaussian_filter1d(np.sum(normtot[:, 0:10], axis=1), 4.8)
plt.plot(transsmooth, 'k-')

# Aggregated 2D profile
plt.figure()
plt.matshow(gaussian_filter(normtot, 4.8), cmap='inferno')   
"""

###########################################################
### Get positions of local maxima in cell-axial profile ###
########################################################### 
"""These lines are useful for finding the local maxima in an RPD profile,
excluding maxima as a results of upward trends at the start/end of the profile:
 
maxfilt = maximum_filter(axtotsmooth, 3)
maxima = (axtotsmooth == maxfilt)
maxima = maxima[1:139]
peaks = np.where(maxima)[0] + 1

m, sdr = fitpeaksnooffset(peaks)
# or
# m, c, sdr = fitpeakswithoffset(peaks)
"""