# -*- coding: utf-8 -*-
"""
Created on Mon Dec 18 10:02:02 2017

@author: fbsacu
"""

import scipy
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.filters import gaussian_filter1d
from scipy.ndimage.filters import maximum_filter
from collections import Counter
from scipy.optimize import minimize
import os
from scipy.optimize import curve_fit
from scipy.stats import ks_2samp
#import tifffile
#from tifffile import imsave

from scipy.io import loadmat
from scipy.special import i0
import time
import modelling_general as model
import relposdensity3d as reld
import pandas as pd


"""
# Import centriole localisations
simul = scipy.io.loadmat('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/Simulated data/Sim_Cep57_9Fold_LE02_woNoise_2017-12-09__10-28-34.mat')
simul = simul['simCent_wNoise'][:, 0] # Since comes out as 100 x 1, not just 100.

dat_152_164 = scipy.io.loadmat('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/Experimental_data/2017-12-01_humanCent_Cep152_Cep164_TopViews.mat')['Top']

dat_192_57 = scipy.io.loadmat('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/Experimental_data/2017-08-22_humanCent_Cep192_Cep57_TopViews.mat')['Top']

simul = loadmat('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/forAlistair_310718/Cep152_A647_Top.mat')
simul = simul['Predicted_Top'][:, 0]

alld = np.zeros((1, 3))
for i in range(24, 33):
    xy = simul[i][simul[i][:, 3] < 5][:, 0:2]
    print xy.shape
    d = getdistances(xy, 500)[0]
    alld = np.append(alld, d, axis=0)
    
    
for i in range(0, 33):
    show = np.histogram2d(simul[i][:, 0], simul[i][:, 1], bins=(100, 100))[0].astype(float)
    show = gaussian_filter(show, sigma=1)
    plt.matshow(show, cmap='inferno')

for i in range(0, 1):
    x = simul[i][:, 0]
    y = simul[i][:, 1]
    show = np.histogram2d(x, y,
            bins=(np.arange(np.min(x) - 5., np.max(x) - 5., 5.),
                np.arange(np.min(y) - 5., np.max(y) - 5., 5.)))[0].astype(float)
    show = gaussian_filter(show, sigma=1)
    plt.matshow(show, cmap='inferno')

simcent = simulatepolygon(generatepolypoints(9, 300), 35, 35, 100)
relxy = getdistances(simcent, 500)[0]
Hsim = hist1d(relxy, 500)

fout = 'S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/forAlistair_310718/Sims/relpos_n%i_dia%i_sd%i_reps%i.npy' % (9, 300, 35, 500)
np.save(fout, relxy)

"""



"""Simulated centrioles are in simul[0:100, 0]
Columns in each simulated centriole (simul[0, 0].shape is e.g. [319, 5])
are [x (nm) , y (nm), z (nm), photons, frame."""

#L = 665
#n = 1.46 # e.g.
#k = n * 2 * np.pi / L
#alpha = np.arcsin(1.45/1.46) # e.g.
# alpha = np.pi/2 # e.g.
#s = 1/(n * k) / np.sqrt((4 - 7 * (np.cos(alpha))**1.5 + 3 * (np.cos(alpha) ** 3.5)) / (7 * (1 - (np.cos(alpha) ** 1.5))))
# s =  # nm, approx.


def get2drelposnofilt(xy):
    """Store all relative positions in a numpy array

    Args:
        xy: numpy array of localisations with shape (N, 2),
        where N is the number of localisations.
    
    Returns:
        Numpy (N, 2) array of 2D relative positions.  
    """
    relpos = xy - xy[0]  
    for i, loc in enumerate(xy[1:len(xy)]):
        relpos = np.append(relpos, xy - loc, axis=0)
        #if i % 500 == 0:
        #    print 'Done loc', i, 'of', len(xy)
    
    # Remove [0., 0.] relative positions (self-referencing)    
    relpos = relpos[np.any(relpos != 0., axis=1)]
    
    return(relpos)
    
def uncert2drelpos(relxy, sx, sy):
    """Provide the uncertainties of the Euclidian distances for
    2d relative positions.
    
    Args:
        relxy: numpy array of relative positions of pairs of localisations
            with shape (N, 2), where N is the number of relative positions.
        sx: uncertainty in x for localisations (not relpos)
        sy: uncertainty in y for localisations (not relpos)
        
    Returns:
        s: Uncertainties of the 2d Euclidian distance    
    """
    s = np.sqrt(2) * np.sqrt(relxy[:, 0] ** 2 * sx ** 2 +
                                 relxy[:, 1] **2 * sy ** 2
                        ) / np.sqrt(relxy[:, 0] ** 2 + relxy[:, 1] ** 2)   
    return s
    

"""

dat = simul
#dat = Cep192
totrelpos = np.array([[0, 0]])
c = 0
hist1d = np.zeros(301)
#for cent in simul:
for cent in dat:
    c = c + 1
    if c % 10 == 0:
        print 'Centriole %i of %i' % (c, len(dat))
    #cent = cent[cent[:, 3] < np.mean(cent[:, 3]) * 2 / 3]
    #cent = cent[cent[:, 3] < 10] # Localisation precision < 10 nm
    centrelpos = get2drelposnofilt(cent[:, 0:2])
    hist1d = np.histogram(
                          np.sqrt(centrelpos[:, 0] ** 2 + centrelpos[:, 1] ** 2),
                          bins=(np.arange(0,302) - 0.5)
                          )[0]
    #plt.figure()
    #plt.title('%i of %i' % (c, len(dat)))
    #plt.plot(hist1d)
    #print 'Saving.'
    #np.save('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/Experimental_data/Cep192relpos1d-prec10nm/hist1d_%i' % ((c - 1)), hist1d)
#print 'Done all centrioles'
    
    totrelpos = np.append(totrelpos, centrelpos, axis=0)
    totrelpos = totrelpos[np.any(totrelpos != 0., axis=1)]
#tothist = np.histogramdd(totrelpos, bins=(range(-350, 351), range(-350, 351)))[0]
#tothist2 = gaussian_filter(tothist, 2)
#plt.matshow(np.sqrt(tothist2), cmap='inferno') 
hist1d = np.histogram(np.sqrt(totrelpos[:, 0] ** 2 + totrelpos[:, 1] ** 2), bins=range(300))[0]
plt.figure()
plt.title('Sum')
plt.plot(hist1d)




dat = simul
#dat = Cep192
totrelpos = np.array([[0, 0]])
c = 0
hist1d = np.zeros(301)
#for cent in simul:
for i, cent in enumerate(dat):
    c = c + 1
    if c % 5 == 0:
        print 'Centriole %i of %i' % (c, len(dat))
    
    if sd[i] < 30:
        cent = cent[cent[:, 3] < 10] # Localisation precision < 10 nm
        centrelpos = get2drelposnofilt(cent[:, 0:2])
        hist1d = hist1d + np.histogram(
                      np.sqrt(centrelpos[:, 0] ** 2 + centrelpos[:, 1] ** 2),
                          bins=(np.arange(0,302) - 0.5)
                          )[0]
        
        totrelpos = np.append(totrelpos, centrelpos, axis=0)
        totrelpos = totrelpos[np.any(totrelpos != 0., axis=1)]
    
#tothist = np.histogramdd(totrelpos, bins=(range(-350, 351), range(-350, 351)))[0]
#tothist2 = gaussian_filter(tothist, 2)
#plt.matshow(np.sqrt(tothist2), cmap='inferno') 
    
hist1d = np.histogram(np.sqrt(totrelpos[:, 0] ** 2 + totrelpos[:, 1] ** 2), bins=range(300))[0]
plt.figure()
plt.title('Sum')
plt.plot(hist1d)
"""


"""
dat = simul
cent = dat[7]
plt.scatter(cent[:, 0], cent[:, 1], marker='+')
"""

def hist1dblurperpoint(relxy, s):
    """Make histogram of 2D Euclidian distances, blurring each point by correct
    uncertainty.
    
    Args:
        relxy: numpy array of relative positions of pairs of localisations
            with shape (N, 2), where N is the number of relative positions.
        s: uncertainty in 2D Euclidian distances of the rel pos'ns
    
    Returns:
        hist: Histogram
    """
    combine = np.column_stack((relxy, s))
    L = 300
    hist = np.zeros(L + 1)
    for i in combine:
        subhist = gaussian_filter1d(
                    np.histogram(np.sqrt(i[0] ** 2 + i[1] ** 2),
                        bins=(np.arange(L + 2) - 0.5))[0].astype(float), i[2])
        hist = hist + subhist
    return hist

def octopoints(d):
    """Generate the coordinates of points on an octagon.
    
    Args:
        d: Diameter of circle on which the vertices are found.
        
    Returns:
        v: Numpy [x, y] coordinates of vertices.
    """
    v = np.zeros((8, 2))
    for a in range(8):
        v[a, 0] = d / 2 * np.cos(a * 2 * np.pi / 8)
        v[a, 1] = d / 2 * np.sin(a * 2 * np.pi / 8)
    
    return (v)

def nonopoints(d):
    """Generate the coordinates of points on an octagon.
    
    Args:
        d: Diameter of circle on which the vertices are found.
        
    Returns:
        v: Numpy [x, y] coordinates of vertices.
    """
    v = np.zeros((9, 2))
    for a in range(9):
        v[a, 0] = d / 2 * np.cos(a * 2 * np.pi / 9)
        v[a, 1] = d / 2 * np.sin(a * 2 * np.pi / 9)
    
    return (v)
    
def generatepolypoints(n, d):
    """Generate the coordinates of points on an polygon.
    
    Args:
        n: Number of vertices.
        d: Diameter of circle on which the vertices are found.
        
    Returns:
        v: Numpy [x, y] coordinates of vertices.
    """
    v = np.zeros((n, 2))
    for a in range(n):
        v[a, 0] = d / 2. * np.cos(a * 2. * np.pi / n)
        v[a, 1] = d / 2. * np.sin(a * 2. * np.pi / n)
    
    return (v)
    
def arcfromline(euc, dia):
    """Get arc length from Euclidian distance and diameter of circle.
    
    Args:
        euc: Euclidian distance between two points.
        dia: Diameter of the circle they are on.
    
    Returns:
        arc: Length of the arc between the two points on the circle.
    """
    arc = dia * np.arcsin(euc / dia)
    return arc
    
def simulatepolygon(polypoints, sx, sy, reps):
    """Provides a simulated polygonal structure with localisation uncertainty.
    
    Args:
        polypoints: Points on an polygon:
                         Numpy (N, 2) array giving N vertices as [x, y]
        sx, sy: Localisation precision in x, y
        reps: Repeated detection of the same point
        
    Returns:
        polysim: Simulated SMLM of an octogon.
    """    
    polysim = polypoints + np.column_stack((
                              np.random.normal(0., sx, len(polypoints)),
                              np.random.normal(0., sy, len(polypoints))
                              ))
    for i in range(1, reps):
        polysim = np.append(polysim,
                    polypoints + np.column_stack((
                              np.random.normal(0., sx, len(polypoints)),
                              np.random.normal(0., sy, len(polypoints))
                              )),
                    axis=0
                    )
    return polysim


"""   
blur = 15 * np.sqrt(2)
d = 230
normfact = 90000
v7 = generatepolypoints(7, d)
v8 = octopoints(d)
v9 = nonopoints(d)
v10 = generatepolypoints(10, d)
septorel = get2drelposnofilt(v7)
octorel = get2drelposnofilt(v8)
nonorel = get2drelposnofilt(v9)
decorel = get2drelposnofilt(v10)
septohist1d = np.histogram(
    np.sqrt(septorel[:, 0] ** 2 + septorel[:, 1] ** 2),
    bins=range(300))[0].astype(float) * normfact * 9/7
septo1dblur = gaussian_filter1d(septohist1d, blur)
octohist1d = np.histogram(
    np.sqrt(octorel[:, 0] ** 2 + octorel[:, 1] ** 2),
    bins=range(300))[0].astype(float) * normfact * 9/8
octo1dblur = gaussian_filter1d(octohist1d, blur)
nonohist1d = np.histogram(
    np.sqrt(nonorel[:, 0] ** 2 + nonorel[:, 1] ** 2),
    bins=range(300))[0].astype(float) * normfact
nono1dblur = gaussian_filter1d(nonohist1d, blur)
decohist1d = np.histogram(
    np.sqrt(decorel[:, 0] ** 2 + decorel[:, 1] ** 2),
    bins=range(300))[0].astype(float) * normfact * 9/10
deco1dblur = gaussian_filter1d(decohist1d, blur)
plt.plot(hist1d, 'k', septo1dblur, 'g--', octo1dblur, 'b--',
            nono1dblur, 'r--', deco1dblur, 'y--')
septodelta = gaussian_filter1d(septohist1d, 1)
octodelta = gaussian_filter1d(octohist1d, 1)
nonodelta = gaussian_filter1d(nonohist1d, 1)
decodelta = gaussian_filter1d(decohist1d, 1)
plt.plot(septodelta / 30, 'g-', octodelta / 30, 'b-',
            nonodelta / 30, 'r-', decodelta / 30, 'y-')
"""



""" ANGULAR ANALYSIS
actot = np.zeros(360)
npeaks = np.array([])
c = 0
#for h in np.where((npeaks <= 7))[0]:
#dat = simul
for i, cent in enumerate(dat): #[np.where((npeaks == 10))[0]]:
    #cent = dat[h]
    c = c + 1
    if c % 10 == 0:
        print 'Centriole %i of %i' % (c, len(dat))
    
    if sd[i] < 35:
    
        # Get r, theta, relatiave to centre of mass
        #centcent = np.array([np.mean(cent[:, 0]), np.mean(cent[:, 1])])
        #centcent = np.array([0, 0])
        #centcent = refcentrelist[c - 1] #[h]
        centcent = refcentrelist[i]
        x = cent[:, 0] - centcent[0]
        y = cent[:, 1] - centcent[1]
        r = np.sqrt(x ** 2 + y ** 2)
        
        # This takes account of Numpy arctan convention
        theta = -np.arctan(y / x) # This may result in an error for zeros in x
           # but it sets the right length, and zeros are addressed below.
        theta[(y == 0)] = 0
        theta[(y == 0) & (x < 0)] = np.pi
        theta[(x == 0) & (y > 0)] = -np.pi / 2
        theta[(x == 0) & (y < 0)] = np.pi / 2
        theta[(x < 0)] = theta[(x < 0)] + np.pi
        
        # Get everything between 0 and 2 * pi
        theta[(theta < 0)] = theta[(theta < 0)] + 2 * np.pi
        
        # Roll around the circle a second time,
        # to get angular separations across the zero point
        theta2 = np.append(theta, theta + 2 * np.pi)
        
        # Make histogram with 1 degree bins
        thetahist = np.histogram(theta2 * 180 / np.pi,
                                   bins=(np.arange(360 * 2 + 2) - 0.5))[0]
        
        # Autocorrelate and plot
        ac = acorrnoshift(thetahist)[0:360]
        
        maxfilt = maximum_filter(ac, 30)
        maxima = (ac == maxfilt)
        npeaks = np.append(npeaks, len(np.where(maxima)[0]))
                
            
        plt.figure()
        plt.title('%i' % i)    
        plt.plot(ac)
        
        actot = actot + ac
    
count = Counter(npeaks)

plt.figure()
plt.title('tot')
plt.plot(actot)

"""

"""
p = np.max(npeaks)
froot = 'S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/Experimental_data/Cep192images-16int/'
fn = 0
stop = np.min(npeaks)
while p >= stop:
    for i in np.where((npeaks == p))[0]:
        print 'Saving centriole', i, '(%i peaks)' % p
        imsave('%scent%i.tif' % (froot, fn),
                   data=np.histogramdd(dat[i][:, 0:2], 
                                       bins=(
                     np.arange(np.min(dat[i][:, 0]) - 50, np.min(dat[i][:, 0]) + 700),
                     np.arange(np.min(dat[i][:, 1]) - 50, np.min(dat[i][:, 1]) + 700)
                                            )
                                      )[0].astype(np.uint16)
               )
        fn = fn + 1
    p = p - 1
"""

def varfromradius(ref, locs, rad):
    """Return sum of (distances from a reference point minus
    known radius of a circle they should be found on)^2.
    
    Args:
        ref: 2D (x, y) reference point.        
        locs: numpy array of localisations with shape (N, 2),
            where N is the number of localisations.
        rad: known radius of circle locs are found on.
         
    Returns:
        sum of )distance between locs and ref)^2
    """
    
    relpos2d = locs - ref
    d = np.sqrt(relpos2d[:, 0] ** 2 + relpos2d[:, 1] ** 2)
    
    return np.sum((d - rad) ** 2)
    
def dfromcentre(centre, locs):
    """Return 1D array of distances from a reference point.
    
    Args:
        ref: 2D (x, y) reference point.        
        locs: numpy array of localisations with shape (N, 2),
            where N is the number of localisations.
        rad: known radius of circle locs are found on.
        
    Returns:
        d: distances from reference point to each localisation.
    """
    relpos2d = locs - centre
    d = np.sqrt(relpos2d[:, 0] ** 2 + relpos2d[:, 1] ** 2)
    
    return d

"""
FIND CENTRE OF CIRCLE FIT FOR LOCS, WITH CENTRE PROVIDING CORRECT RADIUS

c = 0
cent = dat[0]
c = c + 1
xy = cent[:, 0:2]
rad = 230 / 2
CoM = np.mean(xy, axis=0)
refcentre = minimize(varfromradius, x0=CoM, method='Nelder-Mead', args=(xy, rad), tol=1e-3).x
CoMlist = np.array([CoM])
refcentrelist = np.array([refcentre])

for cent in dat[1:]:
    c = c + 1
    if c % 10 == 0:
        print c, 'of', len(dat)
    xy = cent[:, 0:2]
    CoM = np.mean(xy, axis=0)
    refcentre = minimize(varfromradius, x0=CoM, method='Nelder-Mead', args=(xy, rad), tol=1e-3).x
    CoMlist = np.append(CoMlist, np.array([CoM]), axis=0)
    refcentrelist = np.append(refcentrelist, np.array([refcentre]), axis=0)

sd = np.zeros(len(dat))
for i in range(0, len(sd)):
    cent = dat[i]    
    cent = cent[cent[:, 3] < 15] # Localisation precision < 10 nm
    d = dfromcentre(refcentrelist[i], cent[:, 0:2])
    sd[i] = np.sqrt(np.sum((d - np.mean(d)) ** 2) / len(d))
    
    
refcentre = minimize(dfromreference, x0=CoM, method='Nelder-Mead', args=(xy, 230), tol=1e-4)

"""

"""
root = 'S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/Experimental_data/Cep57relpos1d-prec10nm/'
files = os.listdir(root)
tothist = np.zeros(len(np.load(root + files[0])))
for f in files:
    tothist = tothist + np.load(root + f)
plt.plot(tothist)
""" 

def nfoldplusreps(r, dia, sigma, A, sdreps, B):
    n = 6
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n - 1)]
    rpd = np.zeros(len(r))
    for d in dists:
        rpd = rpd + A * model.paircorr2d(r, d, sigma)
    reps = B * model.paircorr2d(r, 0., np.sqrt(2) * sdreps)
    rpd = rpd + reps
    return rpd

def nfoldplusrepsplusbg(r, dia, sigma, A, diaA, bgA, sdreps, B):
    n = 9
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n - 1)]
    rpd = np.zeros(len(r))
    for d in dists:
        rpd = rpd + A * model.paircorr2d(r, d, sigma)
    reps = B * model.paircorr2d(r, 0., np.sqrt(2) * sdreps)
    rpd = rpd + reps
    background = internalbg(r, diaA, bgA)
    rpd = rpd + background
    return rpd

def nfoldplusrepsx2plusbg(r, dia, sigma, A, diaA, bgA , sdreps, B, subsd, C):
    n = 9
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n-1)]
    #contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = np.zeros(len(r))
    for i, d in enumerate(dists):
        rpd = rpd + A * model.paircorr2d(r, d, sigma) #* contribs[i]
    reps = B * model.paircorr2d(r, 0., np.sqrt(2) * sdreps) # r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps
    # Add 2nd set of reps
    reps2 = C * model.paircorr2d(r, 0., np.sqrt(2) * subsd) #r / (2 * subsd ** 2) * np.exp(-(r ** 2) / (4 * subsd ** 2))
    rpd = rpd + reps2
    background = internalbg(r, diaA, bgA)
    rpd = rpd + background
    return rpd

def nfoldplusrepsx2plusbgx2(r, dia, sigma, A, diaA, bgA, diaB, bgB, sdreps, B, subsd, C):
    n = 9
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n-1)]
    #contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = np.zeros(len(r))
    for i, d in enumerate(dists):
        rpd = rpd + A * model.paircorr2d(r, d, sigma) #* contribs[i]
    reps = B * model.paircorr2d(r, 0., np.sqrt(2) * sdreps) # r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps
    # Add 2nd set of reps
    reps2 = C * model.paircorr2d(r, 0., np.sqrt(2) * subsd) #r / (2 * subsd ** 2) * np.exp(-(r ** 2) / (4 * subsd ** 2))
    rpd = rpd + reps2
    background = internalbg(r, diaA, bgA)
    background2 = internalbg(r, diaB, bgB)
    rpd = rpd + background + background2
    return rpd


def nfoldplusrepsx2plusbgsigdep(r, dia, A, diaA, bgA , sdreps, B, subsd, C):
    n = 9
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n-1)]
    sigma = np.sqrt(sdreps ** 2 + subsd ** 2)
    #contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = np.zeros(len(r))
    for i, d in enumerate(dists):
        rpd = rpd + A * model.paircorr2d(r, d, sigma) #* contribs[i]
    reps = B * model.paircorr2d(r, 0., np.sqrt(2) * sdreps) # r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps
    # Add 2nd set of reps
    reps2 = C * model.paircorr2d(r, 0., np.sqrt(2) * subsd) #r / (2 * subsd ** 2) * np.exp(-(r ** 2) / (4 * subsd ** 2))
    rpd = rpd + reps2
    background = internalbg(r, diaA, bgA)
    rpd = rpd + background
    return rpd

def alsofitn(r, n, dia, sigma, A, diaA, bgA , sdreps, B, subsd, C):
    n = np.int(n)
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n-1)]
    #contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = np.zeros(len(r))
    for i, d in enumerate(dists):
        rpd = rpd + A * paircorr2d(r, d, sigma) #* contribs[i]
    reps = B * r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps
    # Add 2nd set of reps
    reps2 = C * r / (2 * subsd ** 2) * np.exp(-(r ** 2) / (4 * subsd ** 2))
    rpd = rpd + reps2
    background = internalbg(r, diaA, bgA)
    rpd = rpd + background
    return rpd

def withpeakratiosclusters(r, #n,
                   dia, sigma, A, sdreps, B, subsd, C,
                   a, b, c, d):
    """
    contribs are contributions of peaks in symmetric fit
    """
    contribs = [a, b, c, d]
    n = 8 # np.int(n) ## DEGREE OF SYMMETRY!!
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n / 2)] # Rounds down, as required
    #contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = np.zeros(len(r))
    for i, d in enumerate(dists):
        rpd = rpd + A * model.paircorr2d(r, d, sigma) * contribs[i]
    reps = B * r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps
    # Add 2nd set of reps
    reps2 = C * r / (2 * subsd ** 2) * np.exp(-(r ** 2) / (4 * subsd ** 2))
    rpd = rpd + reps2
    #background = internalbg(r, dia, bgA)
    #rpd = rpd + background
    return rpd


def withpeakratios(r, #n,
                   dia, sigma, sdreps, B,
                   a, b, c, d):
    """
    contribs are contributions of peaks in symmetric fit
    """
    contribs = [a, b, c, d]
    n = 9 # np.int(n) ## DEGREE OF SYMMETRY!!
    verts = generatepolypoints(n, dia)  
    relpos = get2drelposnofilt(verts)
    dists = np.sqrt(relpos[:, 0] ** 2 + relpos[:, 1] ** 2)
    dists = dists[0:(n / 2)] # Rounds down, as required
    #contribs = np.array([2., 2., 2., 1.])
    #sigma = np.array([sigma0, sigma1, sigma2, sigma3])
    rpd = np.zeros(len(r))
    for i, d in enumerate(dists):
        rpd = rpd + model.paircorr2d(r, d, sigma) * contribs[i]
    reps = B * r / (2 * sdreps ** 2) * np.exp(-(r ** 2) / (4 * sdreps ** 2))
    rpd = rpd + reps

    #background = internalbg(r, dia, bgA)
    #rpd = rpd + background
    return rpd

def RPDcirc(d, R):
    """See journal and lab book.
    Results in amplitude of 1e7
    """
    rpd = 2 * np.pi * d * (
        (np.arccos(d / (2 * R)) -
        np.arctan(d / np.sqrt(4 * R ** 2 - d ** 2)) +
        np.pi / 2
        ) * R ** 2 -
        d * np.sqrt(4 * R ** 2 - d ** 2) / 2
        ) * 10 ** -7
    return rpd

def internalbg(d, dia, A):
    bg = A * RPDcirc(np.arange(np.round(dia)), R=dia/2.)
    rpd = np.pad(bg, (0, len(d) - len(bg)), 'constant')
    return rpd

def internalbgsm(d, dia, A, sigma):
    bg = A * RPDcirc(np.arange(np.round(dia)), R=dia/2.)
    rpd = np.pad(bg, (0, len(d) - len(bg)), 'constant')
    rpd = gaussian_filter1d(rpd, sigma)
    return rpd

def paircorfit(d, dmean, sigma, A):
    return A * model.paircorr2d(d, dmean, sigma)

def internalbgwithpaircorsm(d, dia, A, sepmean, sigma, B):
    bg = A * RPDcirc(np.arange(np.round(dia)), R=dia/2.)
    rpd = np.pad(bg, (0, len(d) - len(bg)), 'constant')
    rpd = rpd + B * model.paircorr2d(d, sepmean, sigma)
    return rpd

def internalbgwithpaircordia(d, dia, A, sigma, B):
    bg = A * RPDcirc(np.arange(np.round(dia)), R=dia/2.)
    rpd = np.pad(bg, (0, len(d) - len(bg)), 'constant')
    rpd = rpd + B * model.paircorr2d(d, dia, sigma)
    return rpd

"""
nns = np.load('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/forAlistair_310718/cents24-32inc_prec5nm_nns500nmfilt_168506.npy')
expd = np.sqrt(nns[:, 0] ** 2 + nns[:, 1] ** 2)
exphist = np.histogram(expd, bins=(np.arange(501.) - 0.5))[0].astype(float)
expscale = exphist / 30000.

popt9, pcov9 = curve_fit(
    nfoldplusrepsx2plusbg, r, expscale, p0=(300., 35., 10., 300., 1., 10., 10., 20., 10.),
    bounds=(0., [400., 100., 100., 490., 10., 50., 50., 100., 100.]))
plt.plot(nfoldplusrepsx2plusbg(r, *popt9))
perr9 = np.sqrt(np.diag(pcov9))
params9 = np.column_stack((popt9, perr9))
ssr9 = np.sum((nfoldplusrepsx2plusbg(r, *popt9) - expscale) ** 2)
ks9 = ks_2samp(expscale, nfoldplusrepsx2plusbg(r, *popt9))

popt = pcov = perr = ssr = ks = np.array([])
plt.plot(expscale)
for n in np.arange(6, 13):
    poptn, pcovn = curve_fit(
        alsofitn, r, expscale, p0=(n, 300., 35., 10., 300., 1., 10., 10., 20., 10.),
        bounds=(0., [20., 400., 100., 100., 490., 10., 50., 50., 100., 100.]))
    plt.plot(alsofitn(r, *poptn))
    perrn = np.sqrt(np.diag(pcovn))
    paramsn = np.column_stack((poptn, perrn))
    ssrn = np.sum((alsofitn(r, *poptn) - expscale) ** 2)
    ksn = ks_2samp(expscale, alsofitn(r, *poptn))
    popt = np.append(popt, poptn)
    perr = np.append(perr, perrn)
    ssr = np.append(ssr, ssrn)
    ks = np.append(ks, ksn, axis=0)


simul = loadmat('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/forAlistair_310718/Cep152_A647_Top.mat')
simul = simul['Predicted_Top'][:, 0]

cols=['centriole', 'n_sym', 'diameter', 'sddia', 'broadening', 'sdbroadening',
    'sdreps1', 'sdsdreps1', 'ampreps1', 'sdampreps1', 'sdreps2', 'sdsdreps2',
    'ampreps2', 'sdampreps2', 'peakamp1', 'sdpeak1', 'peakamp2', 'sdpeak2',
    'peakamp3', 'sdpeak3', 'peakamp4', 'sdpeak4']

# With clusters
p0 = [9.0, 300.0, 35.0, 10., 10., 10., 10., 10.0, 10.0, 10.0, 10.0]
bounds1 = [20.0, 400.0, 1000.0, 50., 1000., 50., 100., 500.0, 500.0, 500.0, 500.0]

# No clusters
#p0 = [9.0, 300.0, 35.0, 10., 10., 10.0, 10.0, 10.0, 10.0]
#bounds1 = [20.0, 400.0, 1000.0, 50., 1000., 500.0, 500.0, 500.0, 500.0]

aiccorrbg = np.array([])
aiccorrbgsm = np.array([])
aiccorrsym = np.array([])

sympars = np.zeros(3) # Store diameter, broadening, loc prec (or cluster size)

relposdf = pd.DataFrame(columns=['cent', 'd'])

extent = 500
for i in range(len(simul)):
    print 'Centriole ', i, '.'
    xy = simul[i][simul[i][:, 3] < 5][:, 0:2]
    d = reld.getdistances(xy, extent)[0]
    df = pd.DataFrame({'cent': i, 'd': np.sqrt(d[:, 0] ** 2 + d[:, 1] ** 2)})
    relposdf = relposdf.append(df)
    print 'Now there are ', relposdf.shape[0], 'distances.'
    
relposdf = pd.read_pickle('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/forAlistair_310718/AllRelpos-500nm-CentrioleIndex.pkl')

## TO FOLLOW ON WITH ONLY CERTAIN CENTRIOLES:
choose005 = [24, 27, 26, 12, 3, 28, 17, 31]
choose01 = [24, 27, 26, 12, 3, 28, 17, 31, 32, 4, 14, 19, 10, 8]
chosen = [3, 10, 12, 17, 24, 25, 26, 27, 29, 31, 32] # 9-fold, rep locs and cluster size, and disc background
relposdf = relposdf.set_index('cent')
dists = relposdf[relposdf.index.isin(chosen)].to_numpy()
H = plt.hist(dists, bins=np.arange(extent + 1),
             weights=np.repeat(1./len(dists) * extent, len(dists)),
             color='xkcd:lightblue', alpha=0.5)[0]

extent = 500

Hscales = np.zeros(extent)
t = time.time()
centnos = np.sort(relposdf.cent.unique())
for i in centnos:
    print '\nCentriole', i
    H = np.histogram(relposdf.d[relposdf.cent == i],
                           bins=range(extent + 1))[0].astype(float)    
    Hscale = H / (np.max(H) / 2)
    Hscales = np.vstack((Hscales, Hscale))
    print time.time() - t, 's so far,'
Hscales = Hscales[1:]

np.savetxt('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/forAlistair_310718/HistogramsPerCentriole-inRows-to500nm.csv', Hscales, delimiter=',')

extent = 500
Hscales = np.loadtxt('S:/Peckham/Bioimaging2/Alistair/Centriole-EPFL/forAlistair_310718/HistogramsPerCentriole-inRows-to500nm.csv', delimiter=',')

## With extra clusters
estparamsdf = pd.DataFrame(columns=['cent', 'diameter', 'broadening', 'repspread1', 'repamp1', 'repspread2', 'repamp2', 'peak1', 'peak2', 'peak3', 'peak4'] )
errparamsdf = pd.DataFrame(columns=['cent', 'diameter', 'broadening', 'repspread1', 'repamp1', 'repspread2', 'repamp2', 'peak1', 'peak2', 'peak3', 'peak4'] )

## No extra clusters
#estparamsdf = pd.DataFrame(columns=['cent', 'diameter', 'broadening', 'repspread1', 'repamp1', 'peak1', 'peak2', 'peak3', 'peak4'] )
#errparamsdf = pd.DataFrame(columns=['cent', 'diameter', 'broadening', 'repspread1', 'repamp1', 'peak1', 'peak2', 'peak3', 'peak4'] )

# No extra clusters
#p0 = [300.0, 35.0, 10., 10., 10.0, 10.0, 10.0, 10.0]
#bounds1 = [400.0, 1000.0, 50., 1000., 500.0, 500.0, 500.0, 500.0]

# With extra clusters
p0 = [300.0, 35.0, 10., 10., 10., 10., 10.0, 10.0, 10.0, 10.0]
bounds1 = [400.0, 1000.0, 50., 1000., 50., 1000., 500.0, 500.0, 500.0, 500.0]

# With 2x background
p0 = [300.0, 35.0, 10., 10., 10., 10., 10.0, 10.0, 10.0, 10.0]
bounds1 = [400.0, 1000.0, 50., 1000., 50., 1000., 500.0, 500.0, 500.0, 500.0]

# With peak ratios
p0 = [300.0, 35.0, 10., 10., 10.0, 10.0, 10.0, 10.0, 2., 2., 2., 2.]
bounds = (0,
    [400.0, 1000.0, 50., 1000., 500.0, 500.0, 500.0, 500.0, 10., 10., 10., 10.])

#t = time.time()

for i, hist in enumerate(Hscales):
    poptsym, pcovsym = curve_fit(
        withpeakratiosclusters, np.arange(extent) + 0.5, hist, p0=p0,
        bounds=(0., bounds1))
    perrsym = np.sqrt(np.diag(pcovsym))
    
    #plt.plot(np.arange(extent) + 0.5, withpeakratioss(np.arange(extent) + 0.5, *poptsym))
    
    poptsym = np.append(i, poptsym)
    perrsym = np.append(i, perrsym)
    
    estparamsdf.loc[i] = poptsym
    errparamsdf.loc[i] = perrsym
    #print time.time() - t, 's so far'    

ratios = errparamsdf / estparamsdf

## Using ratio threshold to select
#ratiothresh = 0.1
#belowthresh = ratios < 0.1
#belowthresh = belowthresh.astype(int)

#keepcents = estparamsdf.loc[(belowthresh.peak1 + belowthresh.peak2 + belowthresh.peak3 + belowthresh.peak4) >= 2]
#keepcents = keepcents.cent
#keepcents = keepcents.astype(int)
#keepcents = list(keepcents)

#for cent in keepcents:
#    plt.plot(np.arange(len(hist)) + 0.5, withpeakratios(np.arange(extent) + 0.5, *estparamsdf[estparamsdf.cent == cent].iloc[0, 1:]))

## Ranking by second lowest peak error / amplitude ratio
sortrats = np.sort(ratios[['peak1', 'peak2', 'peak3', 'peak4']], axis=1)
ratios['secondlowest'] = sortrats[:, 1]
rankedlist = ratios.sort_values(by=['secondlowest']).index.array.astype(int)
rankedval = ratios['secondlowest'].sort_values()


## AIC
# With clusters
p0=[300., 35., 10., 300., 1., 10., 10., 20., 10.]
bounds=(0., [400., 100., 100., 490., 10., 50., 50., 100., 100.])

# With extra background
#p0=[300., 35., 10., 300., 1., 500., 1., 10., 10., 20., 10.]
#bounds=(0., [400., 100., 100., 490., 10., 1000., 10., 50., 50., 100., 100.])

fitlength = extent
r = np.arange(fitlength) + 1
mod = nfoldplusrepsx2plusbg
popt, pcov = curve_fit(
    mod, r, H, p0=p0,
    bounds=bounds)
plt.plot(mod(r, *popt) * np.mean(H))
perr = np.sqrt(np.diag(pcov))
params = np.column_stack((popt, perr))
k = float(len(popt) + 1) # No. free parameters, including var. of residuals
                     # for least squares fit.
ssr = np.sum((mod(r, *popt) - Hscale) ** 2)
aic = fitlength * np.log(ssr / fitlength) + 2 * k
aiccorr = aic + 2 * k * (k + 1) / (fitlength - k - 1)
print 'SSR = ', ssr
print 'AIC =', aic
print 'AICcorr =', aiccorr



# correlation matrix:
pcorr = 1 / np.sqrt(np.transpose(np.diag(pcov)[np.newaxis])) * pcov * 1 /np.sqrt(np.diag(pcov))  

centsdf[(centsdf.sdpeak1 < centsdf.peakamp1 / 2) & (centsdf.sdpeak2 < centsdf.peakamp2 / 2) &
    (centsdf.sdpeak3 < centsdf.peakamp3 / 2) & (centsdf.sdpeak4 < centsdf.peakamp4 / 2)]
"""


""" Plotting figure
from plotting import plotestci

plt.figure()
axall = plt.subplot(121)
axch = plt.subplot(122)
Hall = axall.hist(distsall, bins=np.arange(extent + 1),
             weights=np.repeat(1./len(distsall) * extent, len(distsall)),
             color='xkcd:lightblue', alpha=0.5)[0]
Hch = axch.hist(distsch, bins=np.arange(extent + 1),
             weights=np.repeat(1./len(distsch) * extent, len(distsch)),
             color='xkcd:lightblue', alpha=0.5)[0]

p0=[300., 35., 10., 300., 1., 10., 10., 20., 10.]
bounds=(0., [400., 100., 100., 490., 10., 50., 50., 100., 100.])

# With extra background
#p0=[300., 35., 10., 300., 1., 500., 1., 10., 10., 20., 10.]
#bounds=(0., [400., 100., 100., 490., 10., 1000., 10., 50., 50., 100., 100.])

fitlength = extent
r = np.arange(fitlength) + 0.5
mod = nfoldplusrepsx2plusbg
poptall, pcovall = curve_fit(
    mod, r, Hall, p0=p0,
    bounds=bounds)
perrall = np.sqrt(np.diag(pcovall))

poptch, pcovch = curve_fit(
    mod, r, Hch, p0=p0,
    bounds=bounds)
perrch = np.sqrt(np.diag(pcovch))

plotestci(axall, mod, poptall, perrall, reps=10000, fitlength=extent, ci=95)
plotestci(axch, mod, poptch, perrch, reps=10000, fitlength=extent, ci=95)

"""

"""
# CIs
dists0 = relposdf[relposdf.index.isin([0])].to_numpy()
reps = 5000
boots = model.bootstrap(dists0[:, 0], reps)
H0 = ax0.hist(dists0, bins=np.arange(extent + 1),
             weights=np.repeat(1./len(dists0) * extent, len(dists0)),
             color='xkcd:lightblue', alpha=0.5)[0]
r = np.arange(fitlength) + 1
mod = nfoldplusrepsx2plusbg
popt, pcov = curve_fit(
    mod, r, H0, p0=p0,
    bounds=bounds)
ax0.plot(r, mod(r, *popt), color='xkcd:red', lw=0.75)

poptsamples = np.zeros((reps, len(popt)))
for i in range(reps):
    H = np.histogram(boots[i], bins=np.arange(fitlength + 1), weights=np.repeat(1./len(dists0) * extent, len(dists0)))[0]
    poptsamples[i], pcov = curve_fit(
    mod, r, H,
    p0=(popt), bounds=bounds)
modsamples = np.zeros((len(poptsamples), int(fitlength)))
for i in range(len(poptsamples)):
    modsamples[i] = mod(np.arange(fitlength) + 0.5, *poptsamples[i])
modsort = np.sort(modsamples, axis=0)    

plt.figure()
ax0 = plt.subplot(211)
#After fitting centriole 0, using
dists0 = relposdf[relposdf.index.isin([0])].to_numpy()
H0 = ax0.hist(dists0, bins=np.arange(extent + 1),
             weights=np.repeat(1./len(dists0) * extent, len(dists0)),
             color='xkcd:lightblue', alpha=0.5)[0]
ax0.plot(r, mod(r, *popt), color='xkcd:red', lw=0.75)
ax0.fill_between(r, modsort[int(round(len(modsort) / 100. * (100 - ci) / 2))],
                  modsort[int(round(len(modsort)  / 100. * (ci + (100 - ci) / 2)))],
                  facecolor='xkcd:pink', alpha=0.5)

# Next one is 26 for comparison
ax26 = plt.subplot(212)
dists26 = relposdf[relposdf.index.isin([26])].to_numpy()
H26 = ax26.hist(dists26, bins=np.arange(extent + 1),
             weights=np.repeat(1./len(dists26) * extent, len(dists26)),
             color='xkcd:lightblue', alpha=0.5)[0]
popt26, pcov26 = curve_fit(
    mod, r, H26, p0=p0,
    bounds=bounds)
perr26 = np.sqrt(np.diag(pcov26))
plotestci(ax26, mod, popt26, perr26, reps=10000, fitlength=extent, ci=95)

"""
