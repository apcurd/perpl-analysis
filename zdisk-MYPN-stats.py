# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 16:43:22 2018

@author: fbsacu
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d
import modelling_general as model
from scipy.optimize import curve_fit
from scipy.stats import ks_2samp
from scipy.stats import shapiro



axlength = 201 ## TODO User input for this
x = np.arange(axlength)
relpos = pd.read_pickle('S:/Peckham/Bioimaging/Alistair/PALM-STORM/AlexasNearestNeighbours/Myopalladin/MYPN_NNS_aligned_5_FOVs_len1445698.pkl')

relpos.axial = abs(relpos.axial)
        
# 2D density (1-nm bin histogram) of cell-axial/cell-tranverse relative
# positions
hist, edges = np.histogramdd(np.column_stack(
                  (relpos.transverse, relpos.axial)),
                  bins=(np.arange(0, 251, 1), np.arange(0, 251, 1)))

# Cell-axial profile
ax = np.sum(hist[0:10, 0:(axlength + 1)], axis=0)
ax = ax / 2. # Remove duplicates
axsmooth = gaussian_filter1d(ax, 4.8)

def linrepplusreps5(x, rep, broadening,
                   a, b, c, d, e,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrep5(x, rep, broadening,
                   a, b, c, d, e,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrepplusreps5offset(x, rep, broadening,
                   offset,
                   a, b, c, d, e,# f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]#, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps

    return rpd

def linrep5offset(x, rep, broadening,
                   offset,
                   a, b, c, d, e, #f,
                   #locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]#, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background

    return rpd

def linrep5offsetnoslope(x, rep, broadening,
                   offset,
                   a, b, c, d, e, #f,
                   #locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]#, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    rpd = rpd + bgoffset

    return rpd

def linrep5offsetplusrepsnoslope(x, rep, broadening,
                   offset,
                   a, b, c, d, e, #f,
                   locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]#, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    rpd = rpd + bgoffset
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps

    return rpd

def linrep5noslope(x, rep, broadening,
                   a, b, c, d, e,
                   bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    rpd = rpd + bgoffset
    #reps2 = ampreplocs2 * x / (2 * locprec2 ** 2) * np.exp(
    #                                           -(x ** 2) / (4 * locprec2 ** 2))
    #rpd = rpd + reps2
    return rpd

def linrep6plusrepsoffset(x, rep, broadening,
                          offset,
                   a, b, c, d, e, f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    return rpd

def linrep6offset(x, rep, broadening,
                          offset,
                   a, b, c, d, e, f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    return rpd

def linrep6plusreps(x, rep, broadening,
                   a, b, c, d, e, f,
                   locprec, ampreplocs,
                   bgslope, bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep, broadening)
    
    background = bgoffset + bgslope * x
    rpd = rpd + background
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    return rpd

def linrep6plusrepsoffsetnoslope(x, rep, broadening,
                          offset,
                   a, b, c, d, e, f,
                   locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        if i * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, i * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, i * rep + offset, broadening)
    rpd = rpd + bgoffset
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    return rpd

def linrep6plusrepsnoslope(x, rep, broadening,
                   a, b, c, d, e, f,
                   locprec, ampreplocs,
                   bgoffset
                   ):
    rpd = np.zeros(len(x))
    amps = [a, b, c, d, e, f]
    for i, amp in enumerate(amps):
        if (i + 1) * rep < broadening * 10:
            rpd = rpd + amp * model.paircorr1d(x, (i + 1) * rep + offset, broadening)    
        else:
            rpd = rpd + amp * model.gauss1d(x, (i + 1) * rep + offset, broadening)
    rpd = rpd + bgoffset
    reps = ampreplocs * x / (2 * locprec ** 2) * np.exp(
                                               -(x ** 2) / (4 * locprec ** 2))
    rpd = rpd + reps
    return rpd



fitlength = 90
popt, pcov = curve_fit(
    linrep5offset, x[0:fitlength], axsmooth[0:fitlength],
    p0=(20., 3.4,
        3.,
        1., 1., 1., 1., 1.,# 1.,# 1.,
        #3., 1.,
        -0.02, 20.
        ),
    bounds=([0., 0.,
             0.,
             0., 0., 0., 0., 0.,# 0.,# 0.,
             #0., 0.,
             -100., 0.
             ] ,
        [100., 10.,
         15.,
         1000., 1000., 1000., 1000.,  1000.,# 1000.,# 1000.,
         #20., 1000.,
         100., 100.
         ])
    )
    
#plt.plot(linrepplusreps5offset(x[0:90], *popt))
#plt.plot(linrepplusreps6offset(x[0:110], *popt))
perr = np.sqrt(np.diag(pcov))
params = np.column_stack((popt, perr))
ssr = np.sum((linrepplusreps(x[0:fitlength], *popt) - axsmooth[0:fitlength]) ** 2)

### Linear fit
poptlin, pcovlin = curve_fit(model.linfit, x[0:fitlength],
                             axsmooth[0:fitlength])
perrlin = np.sqrt(np.diag(pcovlin))
paramslin = np.column_stack((poptlin, perrlin))

### Detrend
rpddet = axsmooth[0:fitlength] - model.linfit(x[0:fitlength], *poptlin)
#rpddet = rpddet - np.min(rpddet) # Make above zero

### Bootstrap sampling of relative positions

reps = 100000
boots = model.bootstrap(relpos.axial[relpos.transverse < 10.], reps)


# Calculate bootstrap smoothed profiles
axsmoothb = np.zeros((reps, axlength))
axb = np.zeros((reps, axlength))
for i in range(reps):
    hist, edges = np.histogram(boots[i], bins=(np.arange(0, axlength + 1, 1)))
    axb[i] = hist / 2. # Remove duplicates
    axsmoothb[i] = gaussian_filter1d(axb[i], 4.8)
axsmsort = np.sort(axsmoothb, axis=0)

# Sample model parameters
reps = 100000
poptsamples = np.zeros((reps, len(popt)))
modsamples = np.zeros((reps, fitlength))

for i in range(reps):
    for p in range(len(popt)):
        poptsamples[i, p] = np.random.normal(loc=popt[p], scale=perr[p])
    modsamples[i] = linrep5offsetnoslope(x[0:fitlength], *poptsamples[i])

modsort = np.sort(modsamples, axis=0)

# Detrend RPDs from bootstrapping:
rpddetbsort = np.zeros((reps, fitlength))

for i in range(reps):
    rpddetbsort[i] = axsmsort[i, 0:90] - model.linfit(x[0:fitlength], *poptlin)

statsmypn = np.zeros((reps, 2))
for i in range(reps):
    statsmypn[i] = ks_2samp(rpddetbsort[i, 0:90],
             linrep6plusrepsoffsetnoslope(x[0:fitlength], *popt))
    
float(len(statsmypn[statsmypn[:, 1] > 0.05])) / reps


# Un-detrend!
mod5det = linrep5offsetnoslope(x[0:90], *popt)
lin = model.linfit(x[0:90], *poptlin)
mod5 = mod5det + lin



(rep, broadening, offset,
 a, b, c, d, e, locprec, ampreplocs, bgslope, bgoffset) = popt
axes.plot(bgoffset + bgslope * x)
