# -*- coding: utf-8 -*-
"""
Created on Mon Apr 29 14:52:57 2019

@author: fbsacu
"""

import matplotlib.pyplot as plt
import numpy as np

def plotestci(axes, mod, popt, perr, reps, fitlength, ci, color='xkcd:red'):
## Sample model parameters
    poptsamples = np.zeros((reps, len(popt)))
    modsamples = np.zeros((reps, fitlength))
    
    x = np.arange(fitlength) + 0.5
    
    for i in range(reps):
        for p in range(len(popt)):
            poptsamples[i, p] = np.random.normal(loc=popt[p], scale=perr[p])
        modsamples[i] = mod(x, *poptsamples[i])
    modsort = np.sort(modsamples, axis=0)
    
    axes.plot(x, mod(x, *popt), color=color, lw=0.75)
    axes.fill_between(x, modsort[int(round(len(modsort) / 100. * (100 - ci) / 2))],
                  modsort[int(round(len(modsort)  / 100. * (ci + (100 - ci) / 2)))],
                  facecolor=color, alpha=0.25)

